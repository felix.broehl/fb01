%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script to analyze part 5 of the experiment
%
% for each of several freq bins seperately
% 0.5-2, 1-4, 2-6, 4-8, 8-12, 12-16 Hz
%
% loaded EEG data is sampled at 150 Hz
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clearvars;
addpath('C:\Users\fbroehl\Documents\FB01\analysis\results');
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results';

manageParpool(4);

% parameters
goodsubs = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(goodsubs);
condition = 3; % up to 3 conditions
nfreq = 5;
CHANS = 128;
draws = 50;

% for three conditions
envelopes{1} = [1:3];
envelopes{2} = [1:6];
envelopes{3} = [1:12]; % must fit the size of the envelopes
% for coarse three consitions
Bands{1} = [1;2;3];
Bands{2} = [1:2; 3:4; 5:6];
Bands{3} = [1:4; 5:8; 9:12];
nBands = length(Bands);




%% coarse
alldata = []; alldataHo = [];
for c = 1:condition
    allmi = zeros(nsub,nBands,nfreq,CHANS); 
    allho = zeros(nsub,nBands,nfreq,CHANS,draws);
    argout = {};
    fprintf('compute MI for single envelopes\n');
    tic;
    parfor sub = 1:nsub
        misubslice = zeros(nBands,nfreq,CHANS);
        hosubslice = zeros(nBands,nfreq,CHANS,draws);
        file = goodsubs(sub);
        pause(1);
        for k = 1:3
            envInd = Bands{c}(k,:);
            [data,ARG,ho] = computeMI(file,c,envInd,'true');
            misubslice(k,:,:) = data;
            hosubslice(k,:,:,:) = ho;
            argout{sub} = ARG;
        end
        allmi(sub,:,:,:) = misubslice;
        allho(sub,:,:,:,:) = hosubslice;
    end
    % get ARG.numtrl and assert the trials are complete for each subject
    argout = argout{1}; % this does problems all the time!
    allmi = permute(allmi,[2,1,3,4]);
    alldata = cat(1,alldata,allmi); % con,env,file,freq,chan
    allho = permute(allho,[2,1,3,4,5]);
    alldataHo = cat(1,alldataHo,allho); 
    toc;
end

% save data
sname = sprintf('%s/MI_part5_coarse.mat',savedir);
save(sname, 'alldata','alldataHo','argout');


return




%% coherence function
function [out,ARG,ho] = computeMI(file,condition,USEENVBAND,varargin)
% this function computes the phase coherence value between the trials of
% the chosen condition and the envelope data presented in those trials
%
% by default uses data from EEGPrepro_v3.m aka one ICA over all parts
%
% Parameters:
% file:     
%   files from preprocessed list, integer
% reref:    
%   can be 1 or 2
%   rereferencing eeg data with avereage over all electrodes (1) or
%   mastoid channels (2), integer
% condition:
%   can be 1,2 or 3
%   colculating for either of the three conditions, integer
% USEENVBAND:
%   list indexing which envelopes to be used
%   must fit to the condition - array
% varargin:
%   mode: 'PCV' for phase coherence or 'MI' (default) for mutual information - str
%   randomization: 'true' to create a randomized H0 distribution to test
%   for significance - str
%
% Returns:
% out:
%   either PCV or MI computed between eeg and envelopes
% ARG:
%   specifications in a structure
% ho:
%   if randomization was set to 'true', returns a h0 distribution
%

% directories and paths ---------------------------------------------------
D{1} = 'C:\Users\fbroehl\Documents\FB01\analysis\clean_data';

envdir{1} = 'Z:/FB01/experiment/Stimuli_3_bands/all';
envdir{2} = 'Z:/FB01/experiment/Stimuli_6_bands/all';
envdir{3} = 'Z:/FB01/experiment/Stimuli_12_bands/all';

% create envelope lists
env_list{1} = dir(strcat(envdir{1}, '/*.mat'));
env_list{2} = dir(strcat(envdir{2}, '/*.mat'));
env_list{3} = dir(strcat(envdir{3}, '/*.mat'));

zcopnorm = @(x) copnorm([real(x) imag(x)]);

% get inputs --------------------------------------------------------------
ARG.select = condition;                 % which condition
ARG.USEENVBAND = USEENVBAND;
% get variable inputs
% mode either PCV or MI
if ~isempty(varargin)
    try
        ARG.randomization = char(varargin(1));
    catch
        ARG.randomization = 'false';
    end
end

% set fixed parameters ----------------------------------------------------
ARG.srate = 150;
ARG.prestim = round(0.8 * ARG.srate); % prestimulus time
ARG.soundlag = round(0.03 * ARG.srate); % soundcard lag
ARG.draws = 50; % n Shifts for bootstrapping
ARG.OFFSET = round(0.5 * ARG.srate); % cut out 500 ms from start because of ERPs 
ARG.lags = round([0.06:0.02:0.14] * ARG.srate); % lags in ms (careful that this works with the sampling rate)
nlags = length(ARG.lags);
CHANS = 128;
ARG.refchannel = [46, 57, 58, 59, 104, 118, 119, 120]; % mastoid channels

ARG.freq_range = [0.5, 2; 1,4; 2,6; 4,8; 8,12]; % delta, theta, alpha, beta, gamma
nfreq = length(ARG.freq_range);
ARG.nfreq = nfreq;

% create empty variables
MI_true = zeros(nlags,nfreq,CHANS);
MI_shuf = zeros(nlags,nfreq,CHANS,ARG.draws);

% filters -----------------------------------------------------------------
ARGF.rate = 150;
ARGF.mode = 'butter';
ARGF.order = 4; % filter order
ARGF.window = 1; % symmetric padding

Filt = {};
for b = 1:nfreq
    ARGF.Freqs = ARG.freq_range(b,:);
    tmp = ck_filt_makefilters(ARGF);
    Filt{b} = tmp{1};
end

% get files ---------------------------------------------------------------
clean_list = dir(strcat(D{1}, '/*part_5_CLEAN.mat'));
cond_idx = [ones(39,1); 2*ones(39,1); 3*ones(39,1)];

% load preprocessed data --------------------------------------------------
fprintf('loading file: %s \n', clean_list(file).name);
filename = sprintf('%s/%s', D{1}, clean_list(file).name);
clean_file = load(filename);

% is a cell array because of strings in data structure
sidx = cell2mat(clean_file.index_to_envelope(:,1));
jtrl = find(cond_idx==ARG.select);
numtrl = length(jtrl); % number of present trials
ARG.numtrl = numtrl;

% load trials and concatenate ---------------------------------------------
iter = 1;
env = {};
eeg = {};
for n = 1:numtrl % loop over trials
    %trl = clean_file.TrialInfo(n,1); % because some trials might have been rejected!
    trl = jtrl(n);
    eidx = sidx(trl); % depends on idx legend
    % fetch envelope from correct condition list
    envelope_file = sprintf('%s/%s', env_list{ARG.select}(eidx).folder, env_list{ARG.select}(eidx).name);
    stimulus_all = load(envelope_file);
    
    % fetch eeg and correct envelope
    env{iter} = stimulus_all.env(ARG.USEENVBAND,:); % combine envelopes
    % take mean only if USEENVBAND is list, hence env is matrix
    if length(ARG.USEENVBAND) > 1
        env{iter} = mean(env{iter});
    end
    eeg{iter} = clean_file.data.trial{trl}(1:CHANS,:);
    N_points_env(iter) = length(env{iter});
    N_points_eeg(iter) = length(eeg{iter});
    % reref
    eeg{iter} = eeg{iter}-repmat(mean(eeg{iter}(ARG.refchannel,:),1),[128,1]);
    iter = iter + 1;
end


% save number of trials that will be concatenated
ARG.numtrl = length(eeg);

% for efficient processing, we first append, then filter then cut 
env = [env{:}];
eeg = [eeg{:}];

[nenv,~] = size(env);

% start indices for each envelope
env_segment = cumsum(N_points_env)+1;
env_segment = [1, env_segment(1:end-1)];
% start indices for each eeg trial
onset_segment = cumsum(N_points_eeg)+1;
onset_segment = [1 onset_segment(1:end-1)];


for freq = 1:nfreq % loop over frequency ranges
    % filter and hilbert, phase angle later
    brain = ck_filt_applyfilter(eeg,Filt{freq});
    stim = ck_filt_applyfilter(env,Filt{freq});
    stim = ck_filt_hilbert(stim); % complex signal
    brain = ck_filt_hilbert(brain);
    
    % slice with correct offset over all lags
    for lag = 1:nlags
        LAG = ARG.lags(lag);
        
        % first cut env (remove first 500ms for ERPs) ------
        seglen = sum(N_points_env - ARG.OFFSET);
        seg_env = zeros(nenv,seglen);
        s = 0;
        for n = 1:length(N_points_env)
            I_segment = [1:N_points_env(n)] + env_segment(n)-1;
            I_segment = I_segment(ARG.OFFSET+1:end);
            seg_env(:,s+[1:N_points_env(n)-ARG.OFFSET]) = stim(:,I_segment);
            s = s+N_points_env(n)-ARG.OFFSET;
        end
        
        % then cut eeg accordingly -------------------------
        offset = ARG.prestim + ARG.soundlag + ARG.OFFSET + LAG;
        seg_brain = zeros(CHANS,seglen);
        s = 0;
        
        for n = 1:length(N_points_eeg)
            % for each sentence, we select this setence from the long set
            I_segment = [1:N_points_eeg(n)] + onset_segment(n)-1;
            % -> this segment brain(:,I_segment);
            % now add the lag and take as many elements as we have in the envelope
            I_segment = I_segment(offset:offset+N_points_env(n)-1-ARG.OFFSET);
            seg_brain(:,s+[1:N_points_env(n)-ARG.OFFSET]) = brain(:,I_segment);
            s = s+N_points_env(n)-ARG.OFFSET;
        end
        
        
        % compute MI ---------------------------------------------------------
        S_hilc = zcopnorm(seg_env'); % z-transform
        E_hilc = zcopnorm(seg_brain');
        for e = 1:CHANS
            MI_true(lag,freq,e) = mi_gg(S_hilc,E_hilc(:,e+[0,128]),'true','true');
        end

        % h0 distribution -----------------------------------------------------
        N = ARG.draws;
        switch ARG.randomization
            case {'true'}
                nt = length(seg_env);
                for e = 1:CHANS
                    shift = randi([round(nt/4),round(nt - nt/4)],N,1);
                    for draw = 1:N
                          ivec = [1:nt]; 
                          ivec = circshift(ivec,shift(draw));
                          MI_shuf(lag,freq,e,draw) = mi_gg(S_hilc(ivec,:),E_hilc(:,e+[0,128]),'true','true');
                    end
                end
        end
    end
end

% average/sum over time lags
MI_true = sq(mean(MI_true,1));
MI_shuf = sq(mean(MI_shuf,1));

% output w/ or w/o randomization 
if nargout < 3
    out = MI_true;
elseif nargout > 2
    out = MI_true;
    ho = MI_shuf;
end

end
