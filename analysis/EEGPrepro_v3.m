%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% prepropcessing script - using extended ICA cleaning
%
% There are several possibilities for signal cleaning:
% 'fast'  removes only the most prominent components based on ICA topo
%      (e.g. frontal eye movement related signals and temporal muscle activtiy)
% 'all' which is slower and removes many componets also related to single
%       electrode artifacts, other muscle activity and removes components
%       correlated with EOG signals - REQUIRES PROPER EOG DATA !!!!!!!!!!!!
%
% preprocessing data from all parts of the experiment, append and use
% ICA on combined data for detemination of same components
% then data is split again into all five parts
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;
clearvars;

addpath('Y:\Matlab\ckmatlab\ckstatistics');
addpath('Y:\Matlab\ckmatlab\ckbox');

%----------------------------------------------------------------------------------------
% parameters for preprocessing
%----------------------------------------------------------------------------------------
part = NaN; % analyses only files of that part of the experiment; NaN for no specific file
ARG.Amp_thr = 1000; % threshold for complete trial rejection
ARG.std_fctr = 3.5; % std threshold for bad channel with excessive variance compared to mean std of all channels
ARG.Resample = 150; % sampling rate, use 150 /  200 
ARG.Highpass = 0.2; % high pass filter for preprocessing. Ideal to keep between 0.2 and 0.6

% artifact removal:
ARG.Remove = 'fast'; % 'fast' for  fast processing and removal of eye and muscle artifacts based on topo only
                     % 'eye' for eye movements (EOG) and topos
                     % 'power' for spectra-based and topos
                     % 'all' for all three
ARG.Repair = 'yes';  % automatically select bad channels and interpolate with neighbours
ARG.ICA_artif_thr = 0.7; % threshold: correlation with artifact templates
ARG.ICA_eyecorr = 0.1; % threshold : correlation with eye movement signal (only for 'all') - needs to be titrated
ARG.ICA_spectrum_thr = 3; % power ratio of low to high freq power (only for 'all'). Those with smaller are removed

% EOG processing (used for EOG data cleaning)
ARG.cfg_eog.reog_thr = 2.3; % thresholds for detecting Eye artifacts in EOG data
ARG.cfg_eog.veog_thr = 2.3;
ARG.cfg_eog.reog_win = 0.04; % windowing of EOG artifacts

%--------------------------------------------------------------------------
% Basic params - relative to stimulus onset timing
%--------------------------------------------------------------------------
trialdef.prestim = -0.8; % Trial definition
trialdef.poststim = 0.5; % sec; after trial finished
Trig_range = [1:255]; % triggers to look for (trials). Paradimg specific
CHANS = 128;
LAYOUT = 'biosemi128.lay';
layout = LAYOUT;
mode = 'ICA';   % switch to selectively choose preprocessing or ICA
                % 'all' - do preprocessing, save data, load data and do ICA
                % 'prepro' - only do preprocessing and save to file
                % 'ICA' - only load data, append and apply ICA

%----------------------------------------------------------------------------------------
% set directories and get subject list
%----------------------------------------------------------------------------------------
N100 = 0; % set to false

% directories
preprodir = 'C:\Users\fbroehl\Documents\MATLAB\FB01\analysis\clean_data_mult\prepro';
datadir = 'C:\Users\fbroehl\Documents\MATLAB\FB01\EEG_data';
logdir = 'C:\Users\fbroehl\Documents\MATLAB\FB01\log_data';
savedir = 'C:\Users\fbroehl\Documents\MATLAB\FB01\analysis\clean_data';
envdir = 'Z:\FB01\experiment\Stimuli_3_bands\all';
envdir_block1 = 'Z:\FB01\experiment\Stimuli_3_bands\Block_1';
envdir_block2 = 'Z:\FB01\experiment\Stimuli_3_bands\Block_2';

SubList = ExpList(N100);
Nsub = length(SubList);

%----------------------------------------------------------------------------------------
% load all envelopes (3 bands)
% env_list_part1 is a concatenated list of block1 and then block2
% the indices are in reference to this list!
env_list_all = dir(strcat(envdir, '\*.mat'));
env_list_part1 = [dir(strcat(envdir_block1, '\*.mat')); dir(strcat(envdir_block2, '\*.mat'))];


%%
%----------------------------------------------------------------------------------------
% loop subjects - Some may have multiple files
%----------------------------------------------------------------------------------------
switch mode
    case {'all', 'prepro'}
    % preprocess data and save to prepro dir WITHOUT ICA!
    for S = 1:Nsub
        % in case files are not entered into the list
        if ~isfield(SubList{S},'eeg')
            continue;
        end

          nfiles = length(SubList{S}.eeg);
          for file = 1:nfiles  % try only one file
            if isempty(SubList{S}.eeg{file})
                % no file, again
                continue;
            end

            % skip file if it is not from specified part (s.above)
            if ~isnan(part)
                if ~contains(SubList{S}.eeg{file}, part)
                    continue;
                end
            end
            % skip N100 files for now
            if contains(SubList{S}.eeg{file}, 'N100')
                    continue;
            end

            %-------------------------------------------------------------------------------
            % read events and header file
            %-------------------------------------------------------------------------------
            matfile  = sprintf('%s/%s',logdir,SubList{S}.mat{file});
            bdffile  = sprintf('%s/%s',datadir,SubList{S}.eeg{file});
            if exist(matfile) + exist(bdffile) ~= 4
                fprintf('problem with Subj %d file %d \n',S,file)
                continue;
            end

            fprintf('\n Processing %s \n',bdffile);
            cfg = [];
            cfg.dataset = bdffile;
            event = ft_read_event(cfg.dataset);
            hdr   = ft_read_header(cfg.dataset);

            cfg = [];
            cfg.layout = LAYOUT;
            layout = ft_prepare_layout(cfg);

            %-------------------------------------------------------------------------------
            % get triggers   - THIS MAY BE VERY PARADIGM SPECIFIC
            %-------------------------------------------------------------------------------
            eventS = ft_filter_event(event, 'type','STATUS');
            val = []; t_onset = []; %t_onset_sound = [];
            c=1;
            for k=1:length(eventS)
              if sum(eventS(k).value == Trig_range) && (eventS(k).sample>500)
                val(c) = eventS(k).value;
                t_onset(c) = eventS(k).sample;
                c=c+1;
              end
            end

            %-------------------------------------------------------------------------------
            % add data from .mat file about stimulus condition
            Log = load(matfile);
            % check for complete trials in .mat file
            bgood = find(sum(abs(Log.Result),2));
            Log.Result = Log.Result([bgood],:);
            Log.Timing = Log.Timing([bgood],:);
            % check for consistency between bdf and mat
            n1 = size(Log.Timing,1);
            n2 = length(val);
            if n1~=n2
              warning('Problem with Trial Numbers !!!!!!!');
              fprintf('try to select new trials to match Log \n');
              fprintf('length of t_onset and val must be equal to new number of events \n\n'); 
              fprintf('example: \n');
              fprintf('for the first n trial with n = length(Log.Result) \n\n');
              fprintf('t_onset = t_onset(1:length(Log.Result));\n');
              fprintf('val = val(1:length(Log.Result)); \n');
              keyboard;
              % default to eeg set
            end

            %-------------------------------------------------------------------------------
            % create fieldtrip trial structure - make sure to onset the onset
            % timing not from trial onset but sound onset
            trl = [];
            for t = 1:length(t_onset)
              ts = t_onset(t); % use precise stim onset
              begsample = ts + trialdef.prestim*hdr.Fs;
              %endsample     = ts + trialdef.poststim*hdr.Fs - 1; % time for each trial
              endsample = ts + trialdef.poststim*hdr.Fs + (Log.Timing(t,3)-Log.Timing(t,1))*hdr.Fs;
              offset        = trialdef.prestim*hdr.Fs;
              trl(t,:) = round([begsample, endsample, offset, val(t)]);
            end
            % add Matlab Log file contents
            trl = cat(2,trl,Log.Result(val,:));
            trl = cat(2,trl,Log.Timing(val,:));

            %-------------------------------------------------------------------------------
            % make sure all trials fit within range
            Good_trls = find(trl(:,2)<hdr.nSamples );
            trl = trl(Good_trls,:);

            % select central channels
            switch CHANS
                case 128
                    selchanegobase = eegck_returnEIndex({'A3','A4','A19','A5','A32'},layout.label);
                case 64
                    selchanegobase = eegck_returnEIndex({'Pz','P1','P2'},layout.label);
            end

            %-------------------------------------------------------------------------------
            % detect trials with excessive amplitue based on central electrodes
            %-------------------------------------------------------------------------------
            cfg = [];
            cfg.trl = trl;
            cfg.headerfile = bdffile;
            cfg.datafile = bdffile;
            cfg.continuous = 'yes';
            cfg.artfctdef.threshold.channel = selchanegobase;
            cfg.artfctdef.threshold.bpfilter  = 'yes';
            cfg.artfctdef.threshold.bpfreq    = [1 50];
            cfg.artfctdef.threshold.bpfiltord = 4;
            %cfg.artfctdef.threshold.min  = -ARG.Amp_thr; % default -inf
            cfg.artfctdef.threshold.max  = ARG.Amp_thr;
            [~, artifact_thrs] = ft_artifact_threshold(cfg);

            %-------------------------------------------------------------------------------
            % data processing and loading
            cfg = [];
            cfg.dataset = bdffile;
            cfg.trl = trl;
            cfg.demean = 'no';
            cfg.detrend = 'no';
            cfg.polyremoval = 'no';
            cfg.lpfilter = 'yes';                              % apply lowpass filter
            cfg.lpfreq = 90;                                   % lowpass at 90 Hz.
            cfg.hpfilter = 'yes';                              % apply highpass filter
            cfg.hpfreq = ARG.Highpass;
            cfg.hpfiltord = 4;
            cfg.reref = 'no'; 
            cfg.continuous = 'yes';
            data = ft_preprocessing(cfg);

            %------------------------------------------------------------------
            % remove artifact trials based on FT routines
            cfg = [];
            cfg.artfctdef.reject = 'complete';  % will remove entire trial
            cfg.artfctdef.thr.artifact = artifact_thrs; %
            %data = ft_rejectartifact(cfg,data);

            %-------------------------------------------------------------------------------
            % check if file name contains 'block1' and next file contains
            % 'block2'
            if contains(bdffile, 'block1') 
                if contains(SubList{S}.eeg{file+1}, 'block2')
                    % remember bdf and mat files for next round
                    last_filename = bdffile;
                    data_to_append = data;
                    last_Log = Log;
                    max_sampleinfo = max(data.sampleinfo(:,2)) + 1000; % 1000 for safety
                    continue;
                else
                    warning('there is no next data to append with!');
                    keyboard;
                end
            end

            % if file name contains 'block2' then append with 'block1'
            % append log files
            if contains(bdffile, 'block2')
                cfg = [];
                % try to get data from block1 and delete it afterwards
                try
                    if contains(bdffile, 'part1')
                        data.trialinfo(:,1) = data.trialinfo(:,1) + 95; % 95 is max trl number per block
                    elseif contains(bdffile, 'part4')
                        data.trialinfo(:,1) = data.trialinfo(:,1) + 78; % 78 is max trl number per block
                    end
                    data.sampleinfo = data.sampleinfo + max_sampleinfo;
                    data = ft_appenddata(cfg, data_to_append, data);
                catch
                    warning('there are no previous data to append with!');
                    keyboard;
                end
            end

            % pass sequence from log file and create decent description 
            if contains(bdffile, 'part1')
                Log.ARG.sequence(:,1) = Log.ARG.sequence(:,1) + max(last_Log.ARG.sequence(:,1)); % sentence idx, because 2 lists
                index_to_envelope = [last_Log.ARG.sequence; Log.ARG.sequence];
                index_legend = {'sentence', 'carrier', 'envelope', 'gap', 'gap loc', 'gap len'};
            elseif contains(bdffile, 'part2')
                index_to_envelope = Log.ARG.sequence;
                index_legend = {'condition', 'sentence', 'carrier', 'envelope', 'gap', 'gap loc', 'gap len'};
            elseif contains(bdffile, 'part3')
                index_to_envelope = Log.ARG.sequence;
                index_legend = {'condition', 'sentence', 'carrier', 'envelope', 'gap', 'gap loc', 'gap len'};
            elseif contains(bdffile, 'part4')
                Log.ARG.sequence(:,1) = Log.ARG.sequence(:,1) + 2;
                index_to_envelope = [last_Log.ARG.sequence; Log.ARG.sequence];
                index_legend = {'condition', 'sentence', 'carrier', 'envelope', 'gap low', 'gap loc', 'gap len', 'gap high', 'gap loc', 'gap len'};
            elseif contains(bdffile, 'part5')
                index_to_envelope = Log.ARG.sequence;
                index_legend = {'sentence', 'words', 'index of target'};
            end

            % clear variables after use
            % this ensures that the variables don't contaminate concatenation
            % of other blocks
            clear last_filename;
            clear data_to_append;
            clear last_Log;
            clear max_sampleinfo;

            % index to sentence is in reference to the env_list
            %index_legend = {'sentence'; 'carrier'; 'envelope'};

            %-------------------------------------------------------------------------------
            % find bad channels by excessive variance
            switch ARG.Repair
                case {'yes'}
                    bad_chan_idx = zeros(1,CHANS);
                    for i = 1:size(data.trial,2) % loop trials
                        ref_std = mean(std(data.trial{i}(1:CHANS,:),0,2));
                        for j = 1:CHANS % loop 128 channel, exclude AIB
                            if std(data.trial{i}(j,:)) > ARG.std_fctr * ref_std
                                bad_chan_idx(j) = 1;
                            end
                        end
                    end
                    bad_chan_idx = find(bad_chan_idx);

                    % repair bad channels -------------------------------------
                    repairchannel = bad_chan_idx;
                    cfg = [];
                    LAYOUT = 'biosemi128.lay';
                    cfg.layout = LAYOUT;
                    cfg.method = 'distance'; % for layout
                    layout = ft_prepare_layout(cfg);
                    cfg.neighbours = ft_prepare_neighbours(cfg, data);
                    cfg.method = 'weighted';
                    cfg.trials = 'all';
                    cfg.badchannel = ft_channelselection(repairchannel, data.label);
                    [data] = ft_channelrepair(cfg, data);
                    fprintf('done interpolating %d channels \n', length(repairchannel));
            end

            %-------------------------------------------------------------------------------
            % save trigger channel for clean data
            selchan = ft_channelselection({'Status'}, data.label);
            data_trigger = ft_selectdata(data, 'channel', selchan);

            %-------------------------------------------------------------------------------
            % selct channels of interest (EEG and EOG only) and remove others from
            % FT structure, in case too many channels were recorded
            [data] = eegck_BiosemiEselect(data);

            selchan = ft_channelselection({'all' '-F*' '-G*' '-Temp' '-Resp' '-Plet' '-Erg*' '-EXG8' '-EXG7' '-EXG6' '-EXG5' '-E1*' '-E2*' '-E3*' '-E4*' '-E5*' '-E6*' '-E7*' '-E8*' '-E9*'}, data.label);
            data = ft_selectdata(data, 'channel', selchan); % notice the function's input now with key-value pairs, rather than a cfg.
            selchanegobase = eegck_returnEIndex({'A3','A4','A19','A5','A32'},layout.label);

            %-------------------------------------------------------------------------------
            % Resample 
            cfg            = [];
            cfg.resamplefs = ARG.Resample;
            cfg.detrend    = 'no';
            data           = ft_resampledata(cfg, data);
            data_trigger = ft_resampledata(cfg,data_trigger);

            %-------------------------------------------------------------------------------
            % Compute eye movement signals
            % THIS ASSUMES THAT ALL FOUR EOG ELECTRODES WERE USED
            %-------------------------------------------------------------------------------
            selchan = ft_channelselection({'EXG1' 'EXG2' 'EXG3' 'EXG4'}, data.label);
            data_eog = ft_selectdata(data, 'channel', selchan);
            % VEOG as difference between (EX3 - EX1) and (EX4-EX2)
            % HEOG as difference EX3-EX4
            % REOG as difference mean(EX1-4) - mean (P1,Pz,P2)     % THIS NEEDS HARDCODING FOR EACH ELECTRODE SET
            for t = 1:length(data_eog.trial)
                veog = (data_eog.trial{t}(3,:)- data_eog.trial{t}(1,:))+(data_eog.trial{t}(4,:)- data_eog.trial{t}(2,:));
                veog = veog/2;
                heog = (data_eog.trial{t}(3,:)- data_eog.trial{t}(4,:));
                reog = mean(data_eog.trial{t},1)-mean(data.trial{t}(selchanegobase,:));
                % z-score each
                data_eog.trial{t} = [ck_sig2z(veog);ck_sig2z(heog);ck_sig2z(reog);ck_sig2z(data_eog.trial{t}(1,:));ck_sig2z(data_eog.trial{t}(2,:));ck_sig2z(data_eog.trial{t}(3,:));ck_sig2z(data_eog.trial{t}(4,:))];
            end
                data_eog.label = {'VEOG','HEOG','REOG','EXG1' 'EXG2' 'EXG3' 'EXG4'};





            %------------------------------------------------------------------
            % this is the spot where the preprocessed data must be saved
            %------------------------------------------------------------------

            %-------------------------------------------------------------------------------
            % save file use same name as for bdf file
            sname = sprintf('%s/%s_prepro.mat',preprodir,SubList{S}.eeg{file});
            TrialInfo = data.trialinfo;
            %save(sname,'data','Log','TrialInfo','ARG', 'index_to_envelope', 'index_legend', 'bad_chan_idx');

            keyboard;
          end
    end
end




%% load, append and run ICA on preprocessed data
switch mode
    case {'all', 'ICA'}
    for S = 1:Nsub
        % initialize empty
        DATA = []; LOG = []; trl_idx = [];
        fprintf('loading data of subject %02d\n', S);
        
        % for each subject load all parts that will be appended
        preprolist = dir(strcat(preprodir, '\*.mat'));
        nfiles = length(preprolist);
        
        DATA{1} = load(sprintf('%s/FB01_S%02d_part1_block2.bdf_prepro.mat',preprodir,S));
        DATA{2} = load(sprintf('%s/FB01_S%02d_part2.bdf_prepro.mat',preprodir,S));
        DATA{3} = load(sprintf('%s/FB01_S%02d_part3.bdf_prepro.mat',preprodir,S));
        DATA{4} = load(sprintf('%s/FB01_S%02d_part4_block2.bdf_prepro.mat',preprodir,S));
        DATA{5} = load(sprintf('%s/FB01_S%02d_part5.bdf_prepro.mat',preprodir,S));
        % copy log without data, keep track of number of trials
        for n = 1:5
            trl_idx(n) = length(DATA{n}.data.time); % number of trials
            LOG{n} = DATA{n};
            LOG{n} = removefields(LOG{n}, 'data');
            % only use EEG and EXG channel
            selchan = ft_channelselection({'all', '-Ana*'}, DATA{n}.data.label);
            cfg = [];
            cfg.channel = selchan;
            DATA{n}.data = ft_selectdata(cfg, DATA{n}.data); %, 'channel', selchan);
        end
        % get first trial indices
        trl_segment = [0, cumsum(trl_idx)];
        
        % part 5 has bigger trialinfo because confidence measure is in
        % there (column 7 and 8)
        % we take them out to append data and calculate behaviour solely
        % with log files
        DATA{5}.data.trialinfo = [DATA{5}.data.trialinfo(:,1:6), DATA{5}.data.trialinfo(:,9:12)];
        
        % append parts
        cfg = [];
        alldata = ft_appenddata(cfg, DATA{1}.data, DATA{2}.data, DATA{3}.data, DATA{4}.data, DATA{5}.data);

        % ICA based artifact rejection
        cfg = [];
        cfg.method = 'runica';
        switch ARG.Remove
          case {'fast'}
            % do PCA to speed up process if searching only for frontal topos
            cfg.runica.pca = 40;
        end
        cfg.runica.pca = 40;
        cfg.runica.maxsteps = 130;
        cfg.channel = [1:CHANS]; % this makes sure only eeg channels are used
        comp = ft_componentanalysis(cfg, alldata);

        % display components
        figure(3);
        cfg = [];
        cfg.component = [1:length(comp.label)];       % specify the component(s) that should be plotted
        cfg.layout    = layout; % specify the layout file that should be used for plotting
        cfg.comment   = 'no';
        ft_topoplotIC(cfg, comp)


        %  data browser to see time course THIS MAY HELP TO IDENTIFY ARTIFACTS
        %  BASED ON TIME COURSE - INTERACTIVE
        if 1==0
          cfg = [];
          cfg.channel = [1:4]; % components to be plotted
          cfg.viewmode = 'component';
          cfg.layout = layout; % specify the layout file that should be used for plotting
          cfg.continuous = 'no';
          ft_databrowser(cfg, comp)
        end

        %------------------------------------------------------------------------------
        % automatic component selection based on topography
        Artif_temp = eegck_artiftemp_biosemi(CHANS);
        art_corr = [];
        for l=1:size(comp.topo,2)
          for k=1:size(Artif_temp,2)
            art_corr(l,k) = corr(comp.topo(:,l),Artif_temp(:,k));
          end
        end
        remove_template = find( max(abs(art_corr(:,[1:end])),[],2)>ARG.ICA_artif_thr);
        ARG.ICA.art_corr = art_corr;
        ARG.ICA.remove_template = remove_template;
        %------------------------------------------------------------------------------
        % compute power spectra of components and removme components with a low
        % ratio between low-frequency power and high frequency power
        switch ARG.Remove
          case {'all','power'}
            [Power,fax,ratio] = eegck_componentPSD(comp);
            ARG.ICA.ratio = ratio;
            remove_power1 = find(ratio<ARG.ICA_spectrum_thr);
            good = find(ratio>=ARG.ICA_spectrum_thr);
            % check those with strange power and remove if
            % correlations with templates somewhat high
            remove2 =  find(max(abs(art_corr(remove_power1,:)),[],2)>0.5);
            remove_power = remove_power1(remove2);
            ARG.ICA.remve_power = remove_power;
        end

        %------------------------------------------------------------------------------
        % compute correlations with eye movements and suggest components to be
        % removed
        switch ARG.Remove
          case {'all','eye'}
            EyeAnalysis =  eegck_analyseEOG_v2(data_eog,ARG.cfg_eog);
            M = eegck_trials2mat(comp);
            EyeAnalysis.Saccade  = EyeAnalysis.Saccade';
            EyeAnalysis.Blink  = EyeAnalysis.Blink';
            EyeAnalysis.Exg = EyeAnalysis.Exg';
            CC=[];
            for e=1:size(M,1)
              tmp = (sq(M(e,:,:)));
              tmp = ck_filt(tmp,alldata.fsample,[30,70],'band',6);
              tmp = abs(ck_filt_hilbert(tmp))';
              CC(e,1) = corr(tmp(:),EyeAnalysis.Saccade(:));
              CC(e,2) = corr(tmp(:),EyeAnalysis.Blink(:));
              CC(e,3) = corr(tmp(:),EyeAnalysis.Exg(:));

            end
            ARG.ICA.all_eyecorr = CC;
            thr = std(CC(:));
            CC = max(abs(CC),[],2);
            remove_eye_corr = find((CC>ARG.ICA_eyecorr));
            ARG.ICA.remove_eye_corr = remove_eye_corr;
            M=[];
        end

        %--------------------------------------------------------------
        % collect components to remove
        ARG.ICA.strange_power=[];
        switch ARG.Remove
          case {'fast'}
            remove = remove_template;
          case {'all'}
            % remove based on topo
            remove = [remove_template;remove_power;remove_eye_corr];
            % find those with seem to have strange power but are not rejected and display
            ARG.ICA.strange_power  = setdiff(remove_power,remove);
          case {'eye'}
            remove = [remove_template;remove_eye_corr];
          case {'power'}
            remove = [remove_template;remove_power];
            ARG.ICA.strange_power  = setdiff(remove_power,remove);
        end
        remove = unique(remove);
        remove = sort(remove);
        ARG.ICA.removed_components = remove;

        fprintf('Suggested components to remove:  ');
        for l=1:length(remove)
          fprintf('%d   ',remove(l));
        end
        fprintf('\n');

        % display components
        figure(3);
        chld = get(gcf,'Children');
        ncomp = length(comp.label);
        rejNcomp = length(remove); % number of rejected components
        for l=1:length(remove)
         set(get(chld(ncomp-remove(l)+1),'Title'),'Color',[1 0 0 ])
        end
        for l=1:length(ARG.ICA.strange_power)
         set(get(chld(ncomp-ARG.ICA.strange_power(l)+1),'Title'),'Color',[0 1 0 ])
        end

        %-------------------------------------------------------------------------------
        % remove components
        %-------------------------------------------------------------------------------
        cfg = [];
        cfg.component = remove;
        alldata = ft_rejectcomponent(cfg, comp);
        
        %-------------------------------------------------------------------------------
        % append EEG and EOG data
        %data = ft_appenddata([], data, data_eog);
        %data = ft_appenddata([], data, data_trigger);
        %-------------------------------------------------------------------------------

        close all;
        
        %-------------------------------------------------------------------------------
        % split the data into parts as before
        for n = 1:5 % for each part
            ARG = LOG{n}.ARG;
            Log = LOG{n}.Log;
            TrialInfo = LOG{n}.TrialInfo;
            bad_chan_idx = LOG{n}.bad_chan_idx;
            index_legend = LOG{n}.index_legend;
            index_to_envelope = LOG{n}.index_to_envelope;
            
            % get original trial number
            I_segment = [trl_segment(n)+1 : trl_segment(n+1)];
            
            data = alldata;
            data.time = alldata.time(I_segment);
            data.trial = alldata.trial(I_segment);
            data.trialinfo = alldata.trialinfo(I_segment,:);
            
            % save file use same name as for bdf file
            sname = sprintf('%s/FB01_S%02d_part_%d_CLEAN.mat', savedir, S, n);
            save(sname,'data','Log','TrialInfo','ARG','index_to_envelope','index_legend','bad_chan_idx','rejNcomp');
            
        end
        

    end
end

