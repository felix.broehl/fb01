%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script to analyze part 4 of the experiment
%
% for each of several freq bins seperately
% 0.2-2, 1-4, 2-6, 4-8, 8-12, 12-16 Hz
%
% loaded EEG data is sampled at 150 Hz
%
% calculate MI between each electrode and the envelope
% conditions (env x carrier) are 
% 1: 1,1 + 1,3
% 2: 3,1 + 3,3 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clearvars;
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results';

manageParpool(4);

% parameters
goodsubs = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(goodsubs);
nfreq = 5;
ncons = 4; % 4 for mean, 4 for single
CHANS = 128;
condition = 1:ncons; % for mult Data
USEENVBAND = [1,3];

warning('CAVE: condition numbers must fit to loaded EEG data!');

 
    
%% single
alldata = []; 
for c = condition
    allmi = zeros(nsub,length(USEENVBAND),nfreq,CHANS); 
    argout = {};
    fprintf('compute MI for single envelopes\n');
    tic;
    parfor sub = 1:nsub
        misubslice = zeros(length(USEENVBAND),nfreq,CHANS);
        file = goodsubs(sub);
        pause(1);
        for k = 1:2 % loop over single subbands!
            envInd = USEENVBAND(k);
            [data,ARG] = computeMI(file,c,envInd);
            misubslice(k,:,:) = data;
            argout{sub} = ARG;
        end
        allmi(sub,:,:,:) = misubslice;
    end
    % get ARG.numtrl and assert the trials are complete for each subject
    argout = argout{1}; % this does problems all the time!
    allmi = permute(allmi,[2,1,3,4]);
    alldata = cat(1,alldata,allmi); % con,env,file,freq,chan
    toc;
end


% save data
argout.condition = 'LELC-AL, HEHC-AL, LEHC-AL, HELC-AL, LELC-AH, HEHC-AH, LEHC-AH, HELC-AH';
sname = sprintf('%s/MI_part4.mat',savedir);
save(sname, 'alldata','argout');


return


%% coherence function
function [out,ARG] = computeMI(file,condition,USEENVBAND)
% this function computes the phase coherence value between the trials of
% the chosen condition and the envelope data presented in those trials
%
% by default uses data from EEGPrepro_v3.m aka one ICA over all parts
%
% Parameters:
% file:     
%   files from preprocessed list, integer
% reref:    
%   can be 1 or 2
%   rereferencing eeg data with avereage over all electrodes (1) or
%   mastoid channels (2), integer
% condition:
%   can be 1,2 or 3
%   colculating for either of the three conditions, integer
% USEENVBAND:
%   list indexing which envelopes to be used
%   must fit to the condition - array
%   feed list to average envelopes indexed with list
%   feed integer to use only envelope indexed this way!
% varargin:
%   mode: 'PCV' for phase coherence or 'MI' for mutual information (default) - str
%   randomization: 'false' (default) or 'true' to create a randomized H0 distribution to test
%   for significance - str
%   
%
% Returns:
% out:
%   either PCV or MI computed between eeg and envelopes
%   frequency x channel - for given condition and file
% ARG:
%   specifications in a structure
% ho:
%   if randomization was set to 'true', returns a h0 distribution
%

% directories and paths ---------------------------------------------------
D{1} = 'C:\Users\fbroehl\Documents\FB01\analysis\clean_data';

envdir = 'Z:/FB01/experiment/Stimuli_3_bands/all';

% create envelope lists
env_list = dir(strcat(envdir, '\*.mat'));

zcopnorm = @(x) copnorm([real(x) imag(x)]);

% get inputs --------------------------------------------------------------
ARG.select = condition;                 % which condition
ARG.USEENVBAND = USEENVBAND;
% get variable inputs; mode either PCV (default) or MI
% if ~isempty(varargin)
%     ARG.mode = char(varargin(1));
% else 
%     ARG.mode = 'PCV';
% end
% % randomization for ho distribution
% if length(varargin) > 1
%     ARG.randomization = char(varargin(2));
% else
%     ARG.randomization = 'false';
% end


% set fixed parameters ----------------------------------------------------
ARG.srate = 150;
ARG.prestim = round(0.8 * ARG.srate); % prestimulus time
ARG.soundlag = round(0.03 * ARG.srate); % soundcard lag
ARG.OFFSET = round(0.5 * ARG.srate); % cut out 500 ms from start because of ERPs 
ARG.lags = round([0.06:0.02:0.14] * ARG.srate); % lags in ms (careful that this works with the sampling rate)
nlags = length(ARG.lags);
CHANS = 128;
ARG.refchannel = [46, 57, 58, 59, 104, 118, 119, 120]; % mastoid channels

ARG.freq_range = [0.5, 2; 1,4; 2,6; 4,8; 8,12]; % delta, theta, alpha, beta, gamma
nfreq = length(ARG.freq_range);
ARG.nfreq = nfreq;

% create empty variables
MI_true = zeros(nlags,nfreq,CHANS);

% filters -----------------------------------------------------------------
ARGF.rate = 150;
ARGF.mode = 'butter';
ARGF.order = 4; % filter order
ARGF.window = 1; % symmetric padding

Filt = {};
for b = 1:nfreq
    ARGF.Freqs = ARG.freq_range(b,:);
    tmp = ck_filt_makefilters(ARGF);
    Filt{b} = tmp{1};
end

% get files ---------------------------------------------------------------
clean_list = dir(strcat(D{1}, '/*part_4_CLEAN.mat'));
fprintf('loading file: %s \n', clean_list(file).name);
filename = sprintf('%s/%s', D{1}, clean_list(file).name);
clean_file = load(filename);
data = clean_file.data;
EnvMat = clean_file.index_to_envelope(:,2);
cidx = clean_file.index_to_envelope(:,1);

% is a cell array because of strings in data structure
numtrl = size(data.trial,2); % number of present trials
ARG.numtrl = numtrl;


% load trials and concatenate ---------------------------------------------
iter = 1;
env = {};
eeg = {};
N_points_eeg = [];

for n = 1:numtrl % loop over trials
    trl = clean_file.TrialInfo(n,1); % because some trials might have been rejected! (S07!)
    idx = EnvMat(trl,1);
    
    % find condition of the trial (trlcon == 1 to 5)
    if cidx(trl) ~= ARG.select
        continue;
    end
    
    % fetch envelope from correct condition list
    envelope_file = sprintf('%s/%s', env_list(idx).folder, env_list(idx).name);
    stimulus_all = load(envelope_file);
    
    % fetch eeg and correct envelope
    env{iter} = stimulus_all.env(ARG.USEENVBAND,:); % find envelope
    eeg{iter} = clean_file.data.trial{n}(1:CHANS,:); % trl or n
    N_points_env(iter) = length(env{iter});
    N_points_eeg(iter) = length(eeg{iter});
    % reref
    eeg{iter} = eeg{iter}-repmat(mean(eeg{iter}(ARG.refchannel,:),1),[128,1]);
    iter = iter + 1;
end

% for efficient processing, we first append, then filter then cut 
env = [env{:}];
eeg = [eeg{:}];

[nenv,~] = size(env);

% start indices for each envelope
env_segment = cumsum(N_points_env)+1;
env_segment = [1, env_segment(1:end-1)];
% start indices for each eeg trial
onset_segment = cumsum(N_points_eeg)+1;
onset_segment = [1 onset_segment(1:end-1)];


for freq = 1:nfreq % loop over frequency ranges
    % filter and hilbert, phase angle later
    brain = ck_filt_applyfilter(eeg,Filt{freq});
    stim = ck_filt_applyfilter(env,Filt{freq});
    stim = ck_filt_hilbert(stim); % complex signal
    brain = ck_filt_hilbert(brain);
    
    % slice with correct offset over all lags
    for lag = 1:nlags
        LAG = ARG.lags(lag);
    
        % first cut env (remove first 500ms for ERPs) ------
        seglen = sum(N_points_env - ARG.OFFSET);
        seg_env = zeros(nenv,seglen);
        s = 0;
        for n = 1:length(N_points_env)
            I_segment = [1:N_points_env(n)] + env_segment(n)-1;
            I_segment = I_segment(ARG.OFFSET+1:end);
            seg_env(:,s+[1:N_points_env(n)-ARG.OFFSET]) = stim(:,I_segment);
            s = s+N_points_env(n)-ARG.OFFSET;
        end
        
        % then cut eeg accordingly -------------------------
        offset = ARG.prestim + ARG.soundlag + ARG.OFFSET + LAG;
        seg_brain = zeros(CHANS,seglen);
        s = 0;
        
        for n = 1:length(N_points_eeg)
            % for each sentence, we select this setence from the long set
            I_segment = [1:N_points_eeg(n)] + onset_segment(n)-1;
            % -> this segment brain(:,I_segment);
            % now add the lag and take as many elements as we have in the envelope
            I_segment = I_segment(offset:offset+N_points_env(n)-1-ARG.OFFSET);
            seg_brain(:,s+[1:N_points_env(n)-ARG.OFFSET]) = brain(:,I_segment);
            s = s+N_points_env(n)-ARG.OFFSET;
        end
    
    
        % compute MI -------------------------------------
        S_hilc = zcopnorm(seg_env'); % z-transform
        E_hilc = zcopnorm(seg_brain');
        for e = 1:CHANS
            MI_true(lag,freq,e) = mi_gg(S_hilc,E_hilc(:,e+[0,128]),'true','true');
        end
    
    end
    
end

% output
out = sq(mean(MI_true,1));

end

