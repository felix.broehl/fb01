%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- statistics ---
% generate null hypothesis distribution for part 1 natural vs. unnatural
% condition by random permutation
% find true value in the distribution and find clusters of significant
% electrodes
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;
clearvars;
%addpath('Y:\Matlab\ckmatlab\eegck');
addpath('C:\Users\fbroehl\Documents\FB01\analysis\results');
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results';
finalfigdir = 'C:\Users\fbroehl\Documents\FB01\final_figures';

% parameter ---------------------------------------------------------------
removeSub = 'true';
randomization = 'all'; % 'condition', slope', 'all'
nfreq = 5;
draws = 2000;
CHANS = 128;
group1Con = [2,2]; % natural conditions - int or list
group2Con = [10,10]; % unnatural spectrally rotated conditions
include = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(include);
tmap = zeros(nfreq,CHANS);
ho = zeros(nfreq,CHANS,draws);

% load part 1,2,3 data ----------------------------------------------------
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part1');
Data{1} = MIdata.alldata;
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part2');
Data{2} = MIdata.alldata;
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part3');
Data{3} = MIdata.alldata;
argout = MIdata.argout;

% load data from part 5 single separately ---------------------------------
MIpart5 = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part5_coarse');
Data{4} = MIpart5.alldata;

% part five coarse subband data for final figure
MIdataCoarse = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part5_coarse');
MI_coarse = MIdataCoarse.alldata;


% concatenate all conditions
MI = cat(1, Data{:});



% prepare layout ----------------------------------------------------------
LAYOUT = 'biosemi128.lay';
cfg = [];
cfg.layout = LAYOUT;
layout = ft_prepare_layout(cfg);

% load head cap structure
load FtDummy128
load neighbourStructure_cap128

% custom channel map
load ChanMap
frontcent = eegck_returnEIndex(ChanMap.frontcent,layout.label);
occipital = eegck_returnEIndex(ChanMap.occipital,layout.label);

% load plot zlims
%load topolimit_MI_part5 % to have same zlim per freq range

% define cluster stat parameters ------------------------------------------
cfg = [];
cfg.critvaltype = 'par'; % 'prctile' % type of threshold to apply. Usual 'par'
cfg.critval = abs(tinv(0.01,nsub-1)); % critical cutoff value for cluster members if parametric
cfg.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfg.clusterstatistic = 'maxsum';
cfg.minsize = 3; % minimal cluster size
cfg.pval = 0.01; % threshold to select signifciant clusters
cfg.df = nsub-1;
cfg.neighbours  = neighbours;


%--------------------------------------------------------------------------
% prepare structure for envelope analyses
%--------------------------------------------------------------------------
% create structs with params to loop and create the different figures -----
FigStruc{1}.compList = [1,8,12,15]; % list to fetch conditions
FigStruc{1}.header = 'low env, low car in part 1,3,5 - 4 con';
FigStruc{1}.figname = 'LenvLcarr_part135';
FigStruc{1}.Label = [1,3,5,5]; % only to set text in plot
FigStruc{1}.RegX = [1,2,3,4; 1,1,1,1]'; % set x and intercept for regression 

FigStruc{2}.compList = [3,13,16]; % list to fetch conditions
FigStruc{2}.header = 'mid env, mid car in part 1,5 - 4 con';
FigStruc{2}.figname = 'MenvMcarr_part135';
FigStruc{2}.Label = [1,5,5]; % only to set text in plot
FigStruc{2}.RegX = [1,3,4; 1,1,1]'; % set x and intercept for regression

FigStruc{3}.compList = [5,9,14,17]; % list to fetch conditions
FigStruc{3}.header = 'high env, high car in part 1,3,5 - 4 con';
FigStruc{3}.figname = 'HenvHcarr_part135';
FigStruc{3}.Label = [1,3,5,5]; % only to set text in plot
FigStruc{3}.RegX = [1,2,3,4; 1,1,1,1]'; % set x and intercept for regression

FigStruc{4}.compList = [1,8,12,15,18]; % list to fetch conditions
FigStruc{4}.header = 'low env, low car in part 1,3,5 - Allcon';
FigStruc{4}.figname = 'LenvLcarr_part135_allcon';
FigStruc{4}.Label = [1,3,5,5,5]; % only to set text in plot
FigStruc{4}.RegX = [1,2,3,4,5; 1,1,1,1,1]'; % set x and intercept for regression 

FigStruc{5}.compList = [3,13,16,19]; % list to fetch conditions
FigStruc{5}.header = 'mid env, mid car in part 1,5 - Allcon';
FigStruc{5}.figname = 'MenvMcarr_part135_allcon';
FigStruc{5}.Label = [1,5,5,5]; % only to set text in plot
FigStruc{5}.RegX = [1,3,4,5; 1,1,1,1]'; % set x and intercept for regression

FigStruc{6}.compList = [5,9,14,17,20]; % list to fetch conditions
FigStruc{6}.header = 'high env, high car in part 1,3,5 - Allcon';
FigStruc{6}.figname = 'HenvHcarr_part135_allcon';
FigStruc{6}.Label = [1,3,5,5,5]; % only to set text in plot
FigStruc{6}.RegX = [1,2,3,4,5; 1,1,1,1,1]'; % set x and intercept for regression 


% list for twelche band condition in MI data - only for plotting in the
% errorbars!
twelveBandCond = [18, 19, 20];



%% test speech entrainment against randomization (part 5)
% plot cfg
cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlightchannel = [];
cfg_topo.highlightsize = 9;
cfg_topo.highlightsymbol = '.';
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';

% data 
MItrue = MIdataCoarse.alldata;
MIboot = MIdataCoarse.alldataHo;

avgMItrue = sq(mean(MItrue,2));
avgMIboot = sq(mean(MIboot,2));

% zlim values to plot all topos comparably
zlimMax = max(max(avgMItrue,[],3),[],1);
zlimMin = min(min(avgMItrue,[],3),[],1);

bandnames = {'Env_{low}','Env_{mid}','Env_{high}'};


% parameters for cluster statistics ---------------------------------------
cfgb = [];
cfgb.critvaltype = 'prctile'; % type of threshold to apply. Usual 'par'
cfgb.critval = [1,99]; %abs(tinv(0.05,nsub-1)); %critical cutoff value for cluster members if parametric
cfgb.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfgb.clusterstatistic = 'maxsum';
cfgb.minsize = 3; % minimal cluster size
cfgb.pval = 0.01; % threshold to select signifciant clusters
cfgb.df = nsub-1;
cfgb.neighbours  = neighbours;

cfgb.biasc = 2; % consider bias in this dimension

tic
for c = 1:size(avgMItrue,1)
    X = sq(avgMItrue(c,:,:))';
    XR = permute(sq(avgMIboot(c,:,:,:)),[2,1,3]);
    
    [PosClus{c},NegClus{c}] = eegck_clusterstats_eeg_fb(cfgb,X,XR);
end
toc

% plot
for cond = 1:3
    figure('Position',[500,300,800,600])
    iter = 1;
    for band = 1:3
        for freq = 1:nfreq
            cksp = cksubplot(4,nfreq,iter,1.1);
            pos = cksp.Position;
            cfg_topo.zlim = [zlimMin(freq), zlimMax(freq)];
            connum = band + (cond-1)*3;
            EvpDummy.avg =  sq(avgMItrue(connum,freq,:));
            EvpDummy.time = 0;
            cfg_topo.highlightchannel = [];
            % fetch cluster idx
            posChan = []; 
            if ~isempty(PosClus)
                posChan = find(PosClus{connum}.maskSig(:,freq));
            end
            cfg_topo.highlightchannel = posChan;
            cfg_topo.highlightcolor = 'r';
            ft_topoplotER(cfg_topo,EvpDummy);
            if band == 1
                header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
                title(header, 'FontSize', 14);
            end
            if freq == 1 
                header = sprintf(bandnames{band});
                h = text(-0.8, 0, header, 'FontWeight', 'bold', 'FontSize', 14);
                set(h, 'Rotation',90,'HorizontalAlignment','center');
            end
            if band == 3 
                cbar = colorbar('SouthOutside','FontSize',12');
                cksp.Position = pos;
                if freq == 3
                    cbar.Label.String = 'bits';
                end
            end
            
            drawnow;
            iter = iter + 1;
        end
    end
    header = sprintf('Block 5 - Speech tracking condition %d', cond);
    ckfiguretitle(header);
end





%% final figures mult

rowlabel = {'Env_{low}', 'Env_{high}'};
cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlightchannel = [];
cfg_topo.highlightsize = {9,9};
cfg_topo.highlightsymbol = {'.','.'};
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';
cfg_topo.zlim = [-2.5, 2.5];
            
Cluster = [];
figure('Position',[100,50,1200,800])

% first two rows topoplots ------------------------------------------------
iter = 1;
idxlist = [1,3];
for k = 1:2
    d = idxlist(k);
    % loop for low envelope and high envelope slope stats
    draws = 2000;
    ncomp = length(FigStruc{d}.compList);
    
    % individual beta computation
    compMat = MI(FigStruc{d}.compList,:,:,:);
    Slopes = zeros(nfreq,CHANS,nsub);
    HoSlopes = zeros(nfreq,CHANS,nsub,draws);
    x = FigStruc{d}.RegX;
    for freq = 1:nfreq
        for c = 1:CHANS
            y = sq(compMat(:,:,freq,c));
            dummy = x\y; % mldivide
            Slopes(freq,c,:) = dummy(1,:);
        end
    end
    
    % randomization -------------------------------------------------------
    tic;
    % test slope against mean zero with permutation
    tmap = zeros(nfreq,CHANS);
    ho = zeros(nfreq,CHANS,draws);
    for freq = 1:nfreq
        diff = sq(Slopes(freq,:,:));
        tmap(freq,:) = mean(diff,2)./(std(diff,[],2)./sqrt(nsub));
        y = sq(Slopes(freq,:,:)); % get data
        N = draws;
        for draw = 1:N
            y = y .* (2*randi([0,1],size(y))-1);
            rep = mean(y,2)./(std(y,[],2)./sqrt(nsub));
            ho(freq,:,draw) = rep;
        end
    end
    toc;
            
    
    % ck clusterstats ---------------------------------------------
    fprintf('compute cluster statistics\n');
    tic;
    [PosClus,NegClus] = eegck_clusterstats_eeg(cfg,tmap',permute(ho,[2,1,3]));
    toc;
    Cluster{k} = {PosClus,NegClus};
    
    % print cluster stats and p values
   if ~isempty(PosClus)
        nclus = find(PosClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(PosClus.maskSig==c);
            dpr = PosClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(PosClus.p(c)),max(PosClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end

    if ~isempty(NegClus)
        nclus = find(NegClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(NegClus.maskSig==c);
            dpr = NegClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(NegClus.p(c)),max(NegClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end
    
    % plotting -------------------------------
    for freq = 1:nfreq
        cksubplot(6,6,6*(iter-1)+freq,1.1);
        EvpDummy.avg =  tmap(freq,:)';
        EvpDummy.time = 0;
        cfg_topo.highlightchannel = [];
        % fetch cluster idx
        posChan = []; negChan = [];
        if ~isempty(PosClus)
            posChan = find(PosClus.maskSig(:,freq));
        end
        if ~isempty(NegClus)
            negChan = find(NegClus.maskSig(:,freq));
        end
        cfg_topo.highlightchannel = {posChan; negChan};
        cfg_topo.highlightcolor = {'r', 'w'};
        ft_topoplotER(cfg_topo,EvpDummy);
        if iter == 1
            header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
            title(header, 'FontSize', 14);
        end
        if freq == 1
            header = sprintf(rowlabel{iter});
            text(-1.5, 0, header, 'FontWeight', 'bold', 'FontSize',15);
        end
        drawnow;
    end
    
    % plot individual diff values averaged over cluster electrodes
    PosData = []; NegData = []; nclusP = 0; nclusN = 0;
    if ~isempty(PosClus)
        nclusP = length(find(PosClus.p));
        PosData = zeros(nsub,nclusP);
        for c = 1:nclusP
            cluschan = PosClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            B = Slopes(cluschan);
            B = reshape(B,[clussize,nsub]);
            PosData(:,c) = mean(B);
        end
    end
    if ~isempty(NegClus)
        nclusN = length(find(NegClus.p));
        NegData = zeros(nsub,nclusN);
        for c = 1:nclusN
            cluschan = NegClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            B = Slopes(cluschan);
            B = reshape(B,[clussize,nsub]);
            NegData(:,c) = mean(B);
        end
    end
    % subplot to far right
    if ~isempty(PosClus) || ~isempty(NegClus)
        cksubplot(6,6,6 + 6*(k-1),1.0);
        hold on
        nclus = nclusP + nclusN;
        line([-0.5,nclus+0.5],[0,0],'Color','k', 'LineWidth', 0.8);
        if nclusP > 0
            jitter = 0.2 .* (rand(size(PosData)) - 0.5);
            x = repmat([1:nclusP],size(PosData,1),1);
            x = x + jitter;
            plot(x,PosData,'r.','MarkerSize',7);
        end
        if nclusN > 0
            jitter = 0.2 .* (rand(size(NegData)) - 0.5);
            x = repmat([1:nclusN],size(NegData,1),1);
            x = x + nclusP + jitter;
            plot(x,NegData,'b.','MarkerSize',7);
        end
        xlim([0.5,8.5]);
        set(gca,'box','off');
        set(get(gca,'XAxis'),'visible','off');
        ticks = get(gca,'YTick');
        ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
        yticks(ticks);
    end
    
    iter = iter+1;
end




% next two rows topoplots -------------------------------------------------
% statistical difference between high and low env tracking in all conditions
% ENVhigh - ENVlow for all conditions
% analogous to part 1

envlabel = '?Env';
rowlabel = {'3Bands','6Bands'}; 

%figure('Position',[200,200,1200,800])
for k = 1:2 % condition loop
    tmap = zeros(nfreq,CHANS);
    ho = zeros(nfreq,CHANS,draws);
    group1con = 1 + (k-1)*3;
    group2con = 3 + (k-1)*3;
    group1 = sq(mean(MI_coarse(group1con,:,:,:),1));
    group2 = sq(mean(MI_coarse(group2con,:,:,:),1));
    diffAll = group2 - group1;
    tic;
    for freq = 1:nfreq
        diff = sq(diffAll(:,freq,:));
        tmap(freq,:) = mean(diff,1)./(std(diff,[],1)./sqrt(nsub));

        % random permuation ---------------------------------------------------
        N = draws;
        for draw = 1:N
            % invert sign randomly
            hodiff = diff .* (2*randi([0,1],size(diff))-1);
            ho(freq,:,draw) = mean(hodiff,1)./(std(hodiff,[],1)/sqrt(nsub));
        end
    end
    toc;
    
    tic;
    [PosClus,NegClus] = eegck_clusterstats_eeg(cfg,tmap',permute(ho,[2,1,3]));
    toc;
    Cluster{k+2} = {PosClus,NegClus};
    
    % print cluster stats and p value
    if ~isempty(PosClus)
        nclus = find(PosClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(PosClus.maskSig==c);
            dpr = PosClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(PosClus.p(c)),max(PosClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end

    if ~isempty(NegClus)
        nclus = find(NegClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(NegClus.maskSig==c);
            dpr = NegClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(NegClus.p(c)),max(NegClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end
    
    % plotting
    for freq = 1:nfreq
        cksubplot(6,6,12 + 6*(k-1) + freq,1.1);
        EvpDummy.avg =  tmap(freq,:)';
        EvpDummy.time = 0;
        cfg_topo.highlightchannel = [];
        % fetch cluster idx
        posChan = []; negChan = [];
        if ~isempty(PosClus)
            posChan = find(PosClus.maskSig(:,freq));
        end
        if ~isempty(NegClus)
            negChan = find(NegClus.maskSig(:,freq));
        end
        cfg_topo.highlightchannel = {posChan; negChan};
        cfg_topo.highlightcolor = {'r', 'w'};
        ft_topoplotER(cfg_topo,EvpDummy);
        if freq == 1
            %header = sprintf(rowlabel{k});
            %text(-1.5, 0, [envlabel newline rowlabel{k}], 'FontWeight', 'bold', 'FontSize',15);
            text(-1.5, 0, rowlabel{k}, 'FontWeight', 'bold', 'FontSize',15);
        end
        drawnow;
    end
    
    % plot individual diff values averaged over cluster electrodes
    PosData = []; NegData = [];  nclusP = 0; nclusN = 0;
    if ~isempty(PosClus)
        nclusP = length(find(PosClus.p));
        PosData = zeros(nsub,nclusP);
        for c = 1:nclusP
            cluschan = PosClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            PosData(:,c) = mean(B);
        end
    end
    if ~isempty(NegClus)
        nclusN = length(find(NegClus.p));
        NegData = zeros(nsub,nclusN);
        for c = 1:nclusN
            cluschan = NegClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            NegData(:,c) = mean(B);
        end
    end
    % subplot to far right
    if ~isempty(PosClus) || ~isempty(NegClus)
        cksubplot(6,6,18 + 6*(k-1),1.0);
        hold on
        nclus = nclusP + nclusN;
        line([-0.5,nclus+0.5],[0,0],'Color','k', 'LineWidth', 0.8);
        if nclusP > 0
            jitter = 0.2 .* (rand(size(PosData)) - 0.5);
            x = repmat([1:nclusP],size(PosData,1),1);
            x = x + jitter;
            plot(x,PosData,'r.','MarkerSize',7);
        end
        if nclusN > 0
            jitter = 0.2 .* (rand(size(NegData)) - 0.5);
            x = repmat([1:nclusN],size(NegData,1),1);
            x = x + nclusP + jitter;
            plot(x,NegData,'b.','MarkerSize',7);
        end
        xlim([0.5,8.5]);
        set(gca,'box','off');
        set(get(gca,'XAxis'),'visible','off');
        ticks = get(gca,'YTick');
        ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
        yticks(ticks);
    end
    
end
pos = get(gca,'position');
cbar = colorbar('SouthOutside','Limits',[-2.5,2.5],'Ticks',[-2.5,2.5],'TickLabels',[-2.5,2.5], 'FontSize', 12,'FontWeight', 'bold');
cbar.Label.String = 't-value';
caxis([-2.5,2.5]);
set(gca,'position',pos);
width = cbar.Position(3);
width = width*2/3;
cbar.Position(3) = width;




% next rows errorbars -----------------------------------------------------

% MI trajectory for all three envelopes in one figure
% create two separate figures for clarity
label = {'fr','oc'};
% from violinplot in freq_analysis_envelopes.m
barcolor = [0,0.5,0.75; 0.2,0.5,0.2; 0.55,0.2,0.42];
ebjitter = [-0.1, 0, 0.1]; % to make 12 bands data point more visible
hold on
for d = 1:3 % plot only full errorbars
    % loop for low envelope and high envelope slope stats
    xAxis = FigStruc{d}.RegX(:,1)';
    compMat = MI(FigStruc{d}.compList,:,:,frontcent);
    
    % first four conditions
    frdataAvg = sq(mean(mean(compMat,4),2));
    % SEM across subjects
    data = sq(mean(compMat,4));
    frdataSEM = sq(std(data,[],2)/sqrt(nsub));
    
    % add twelve Bands MI to error bar
    % only for plotting in figure - no statistics!!
    tBdMI = MI(twelveBandCond(d),:,:,frontcent);
    tBdMIAvg = sq(mean(mean(tBdMI,4),2));
    tmp = mean(tBdMI,4);
    tBdMISEM = sq(std(tmp,[],2)/sqrt(nsub));
    
    iter = 1;
    for freq = 1:nfreq
        % actual plotting -------------------------------------------------
        cksubplot(6,6,[24+freq, 30+freq],1.0); % third row)
        hold on
        if d == 2 % mid env
            errorbar(xAxis+ebjitter(d),frdataAvg(:,freq),frdataSEM(:,freq),'--','Color',barcolor(d,:), 'LineWidth',1.5);
        else
            errorbar(xAxis+ebjitter(d),frdataAvg(:,freq),frdataSEM(:,freq),'Color',barcolor(d,:), 'LineWidth',2.5);
        end
        errorbar(5+ebjitter(d),tBdMIAvg(freq),tBdMISEM(freq),'o', 'Color',barcolor(d,:), 'LineWidth',1.5);
        xticks([1:5]);
        xticklabels([1,2,3,6,12]);
        xlim([0.5,5 + 0.5]);
        if freq == 1
            xlabel('number of bands', 'FontWeight', 'bold');
            ylabel('MI', 'FontWeight', 'bold');
            header = sprintf(label{1});
        end
        grid off
        if d == 3
            % only min max yticks
            ticks = get(gca,'YTick');
            ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
            yticks(ticks);
        end
        drawnow;
        iter = iter + 1;
    end
end

% set one legend outside
pos = get(gca,'position');
legendlabel = {'low','mid','high'};
legend(legendlabel, 'Location', 'southeastoutside');
set(gca,'position',pos);


sname = sprintf('%s/_Fig7',finalfigdir);
% savefig(sname);
pngname = sprintf('%s.png',sname);
% saveas(gcf,pngname);

snameCluster = sprintf('%s/Clusterstats_Fig7',finalfigdir);
% save(snameCluster,'Cluster');


%% plot selected channel for figure inlays
cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'nearest'; 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.colormap = [1,1,1;1,1,1]; % all white background
cfg_topo.marker = 'off';
cfg_topo.highlightsize = 15;
cfg_topo.highlightsymbol = '.';
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';
% empty data
EvpDummy.avg =  zeros(128,1);
EvpDummy.time = 0;
                
figure
cfg_topo.highlightchannel = frontcent;
cfg_topo.highlightcolor = [1,0,0];
ft_topoplotER(cfg_topo, EvpDummy);

figure
cfg_topo.highlightchannel = occipital;
cfg_topo.highlightcolor = [0,0,1];
ft_topoplotER(cfg_topo, EvpDummy);

                
