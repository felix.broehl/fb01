%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- statistics ---
% part 1 of FB01
% analyzes of behavioral response, i.e. % correct trials
% plot the tracking of the envelopes and carriers, then computes the
% correlation of the topographies between frequencies and stimuli
% then compares the conditions with a cluster based permutation test
%
% in the end creates the final plots for submission
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;
clearvars;
addpath('Y:\Matlab\ckmatlab\eegck');
addpath('C:\Users\fbroehl\Documents\FB01\analysis');
addpath('C:\Users\fbroehl\Documents\FB01\analysis\results\');
bootdir = 'C:\Users\fbroehl\Documents\FB01\analysis\results_boot\';
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results\';
%finalfigdir = 'C:\Users\fbroehl\Documents\FB01\final_figures';

% loading -----------------------------------------------------------------
removeSub = 'true';
randomization = 'all'; % 'condition', slope', 'all'
include = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(include);
nfreq = 5;

MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part1');
MI = MIdata.alldata;
MIboot = MIdata.alldataHo;

% parameter ---------------------------------------------------------------
argout = MIdata.argout;
tscore_thrs = tinv(0.99,nsub-1); % inverse of t cumulative distribution with p of 0.95 and 20 degrees of freedom (nsub-1)
draws = 2000;
CHANS = 128;

% prepare layout ----------------------------------------------------------
LAYOUT = 'biosemi128.lay';
cfg = [];
cfg.layout = LAYOUT;
layout = ft_prepare_layout(cfg);

% load head cap structure
load FtDummy128
load neighbourStructure_cap128

% custom channel map
load ChanMap
frontcent = eegck_returnEIndex(ChanMap.frontcent,layout.label);
occipital = eegck_returnEIndex(ChanMap.occipital,layout.label);

% parameters for cluster statistics ---------------------------------------
cfg = [];
cfg.critvaltype = 'par'; %'prctile' % type of threshold to apply. Usual 'par'
cfg.critval = abs(tinv(0.01,nsub-1)); % critical cutoff value for cluster members if parametric
cfg.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfg.clusterstatistic = 'maxsum';
cfg.minsize = 3; % minimal cluster size
cfg.pval = 0.01; % threshold to select signifciant clusters
cfg.df = nsub-1;
cfg.neighbours  = neighbours;



%--------------------------------------------------------------------------
% prepare structure for envelope analyses
%--------------------------------------------------------------------------
% struct with parameters to loop over for all analyses
% Group 2 MINUS Group 1
StatStruc{1}.group1 = [1,2];
StatStruc{1}.group2 = [4,5];
StatStruc{1}.label = 'Comp Env mean Car';
StatStruc{1}.sname = 'Comp_Env_mean_Car';

StatStruc{2}.group1 = [1,4];
StatStruc{2}.group2 = [2,5];
StatStruc{2}.label = 'Comp Car mean Env';
StatStruc{2}.sname = 'Comp_Car_mean_Env';

StatStruc{3}.group1 = [1,5];
StatStruc{3}.group2 = [2,4];
StatStruc{3}.label = 'rotated minus natural';
StatStruc{3}.sname = 'Comp_Rot_Nat';

StatStruc{4}.group1 = 1;
StatStruc{4}.group2 = 4;
StatStruc{4}.label = 'con4-con1';
StatStruc{4}.sname = 'con4-con1';

StatStruc{5}.group1 = 4;
StatStruc{5}.group2 = 5;
StatStruc{5}.label = 'con5-con4';
StatStruc{5}.sname = 'con5-con4';

StatStruc{6}.group1 = 2;
StatStruc{6}.group2 = 5;
StatStruc{6}.label = 'con5-con2';
StatStruc{6}.sname = 'con5-con2';

StatStruc{7}.group1 = 1;
StatStruc{7}.group2 = 2;
StatStruc{7}.label = 'con2-con1';
StatStruc{7}.sname = 'con2-con1';


%% load bootstrapping data
ncon = 5;
MI = zeros(nsub,ncon,nfreq,CHANS);
MIboot = zeros(nsub,ncon,nfreq,CHANS,draws);

for isub = 1:nsub
    %isub = include(isub2);
    filename = sprintf('%sMI_part1_sub%02d.mat',bootdir,isub);
    data = load(filename);
    MI(isub,:,:,:) = data.allmi;
    MIboot(isub,:,:,:,:) = data.allho;
end

MI = permute(MI,[2,1,3,4]);
MIboot = permute(MIboot,[2,1,3,4,5]);

%% sort and average MI values by conditions
VAR = [];
VAR(1,:,:,:) = sq(mean(MI(1:4,:,:,:)));
VAR(2,:,:,:) = sq(mean(MI(2:5,:,:,:)));
VAR(3,:,:,:) = sq(mean(MI(1:2,:,:,:)));
VAR(4,:,:,:) = sq(mean(MI(4:5,:,:,:)));
meanVAR = sq(mean(VAR,2));

VARboot = [];
VARboot(1,:,:,:,:) = sq(mean(MIboot(1:4,:,:,:,:)));
VARboot(2,:,:,:,:) = sq(mean(MIboot(2:5,:,:,:,:)));
VARboot(3,:,:,:,:) = sq(mean(MIboot(1:2,:,:,:,:)));
VARboot(4,:,:,:,:) = sq(mean(MIboot(4:5,:,:,:,:)));


% parameters for cluster statistics ---------------------------------------
cfgb = [];
cfgb.critvaltype = 'prctile'; % type of threshold to apply. Usual 'par'
cfgb.critval = [1,99]; %abs(tinv(0.05,nsub-1)); %critical cutoff value for cluster members if parametric
cfgb.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfgb.clusterstatistic = 'maxsum';
cfgb.minsize = 3; % minimal cluster size
cfgb.pval = 0.01; % threshold to select signifciant clusters
cfgb.df = nsub-1;
cfgb.neighbours  = neighbours;

cfgb.biasc = 2; % consider bias along this dimension in input data


avgVARtrue = sq(mean(VAR,2));
avgVARboot = sq(mean(VARboot,2));

PosClus = {}; NegClus = {};
for c = 1:size(VAR,1)
    X = sq(avgVARtrue(c,:,:))';
    XR = permute(sq(avgVARboot(c,:,:,:)),[2,1,3]);
    [PosClus{c},NegClus{c}] = eegck_clusterstats_eeg_fb(cfgb,X,XR);
end

% freq labels for plot
t = num2cell(argout.freq_range);
C = cellfun(@num2str, t, 'UniformOutput', false);
C = join(C,' - ');
T{1} = '0.5 - 2';
T = [T;C]; % does not work only with C, who knows why...


snamef = sprintf('%sFig3_cluster',savedir);
% save(snamef,'PosClus','NegClus');


%% final figure part 1 fig 1
% restrict up to 12 Hz (five topos per row)
% A - topos env and car
% B - correlation similarity matrices fro freq and env/car

cfg_topo = [];
cfg_topo.layout = 'biosemi128.lay';
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.highlight          = 'off';
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlight = 'off';
cfg_topo.highlightsymbol = 'o';
cfg_topo.highlightsize = 2;
cfg_topo.shading = 'flat';

% zlim values to plot all topos comparably
zlimMax = max(max(meanVAR,[],3),[],1);
zlimMin = min(min(meanVAR,[],3),[],1);

figure('Position',[500,350,900,600]) % set window size
str = {'Car_{low}';'Car_{high}';'Env_{low}';'Env_{high}'};
iter = 1;
for c = 1:size(VAR,1)
    for b = 1:nfreq
        %cksubplot(4,nfreq+1,iter,1.1);
        handle = cksubplot(5,nfreq,iter,1.1);
        pos = get(handle,'position');
        cfg_topo.zlim = [zlimMin(b), zlimMax(b)];
        EvpDummy.avg =  sq(meanVAR(c,b,:));
        EvpDummy.time = 0;
        
        % highlight pos channels
        %posChan = find(PosClus{c}{b}.maskSig);
        %posChan = find(PosClus{c}.maskSig(:,b));
        %cfg_topo.highlightchannel = posChan;
        %cfg_topo.highlightcolor = 'r';
        
        ft_topoplotER(cfg_topo,EvpDummy);
        if c == 1
            header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(b,1), argout.freq_range(b,2));
            title(header, 'FontSize', 14);
        end
        if b == 1 
            header = sprintf(str{c});
            text(-1.45, 0, header, 'FontWeight', 'bold', 'FontSize', 14);
        end
        % add horizontal colorbar
        if c == size(VAR,1)
            ticks = arrayfun(@(x) sprintf('%.2g',x),[zlimMin(b), zlimMax(b)],'UniformOutput',false);
            cbar = colorbar('SouthOutside','Ticks',[zlimMin(b), zlimMax(b)],'TickLabels',ticks, 'FontSize',12, 'FontWeight', 'bold');
            %width = cbar.Position(3)/8;
            %cbar.Position(1) = cbar.Position(1) + width;
            %cbar.Position(3) = cbar.Position(3) - 2*width;
            if b == 3
                cbar.Label.String = 'Mutual Information (MI)';
            end
            set(handle,'position',pos);
        end
        iter = iter + 1;
        drawnow;
    end
end


% save figure
sname = sprintf('%s_Fig3A',savedir);
print('-dpng',sname);





% fig 1 B - topo correlations ---------------------------------------------
nenv = size(VAR,1); % 4 - two envelopes, two carriers
Corr = zeros(nsub,nfreq,nfreq);
VARtmp = sq(mean(VAR,1)); % average env/car because we arent interested
for s = 1:nsub
    topos = sq(VARtmp(s,:,:))';
    %topos = (topos - min(topos,[],2)) ./ (max(topos,[],2) - min(topos,[],2));
    Corr(s,:,:) = corr(topos); % freq x freq mat of corrcoef
end
CorrZ = corr2z(Corr); % fisher transform

% randomize creates distribution of means 
% check confidence intervals if they exclude zero
hoCorrZ = zeros(draws,nfreq,nfreq);
tic;
for draw = 1:draws
    dcorr = zeros(nsub,nfreq,nfreq);
    ridx = randi(nsub,1,nsub); % get random index with replacement
    for n = 1:nsub
        rtopos = sq(mean(VAR(:,ridx(n),:,:),1))';
        dcorr(n,:,:) = corr(rtopos);
    end
    dcorr = corr2z(dcorr);
    hoCorrZ(draw,:,:) = sq(mean(dcorr,1));
end
% correlation at 0.01 significance level!
% null dist of correlations must exclude 0 at the LOWER end
prcHo = sq(prctile(hoCorrZ,1,1));
toc;

ciHo = prcHo > 0; % find all cells with CI that exclude 0


% print results
fprintf('------------------------------------- \n \n');
fprintf('Frequency topography correlations: \n');
sq(mean(Corr,1))
fprintf('\n');
fprintf('significance label: \n');
ciHo
fprintf('\n\n');


% plot correlation matrix
finalCorr = Corr;
finalCiHo = ciHo;

topocorrFreq = sq(mean(finalCorr,1));
figure('Position',[500,350,900,600]) % set window size
N = size(finalCorr,2); % 5 in this case
M = topocorrFreq;
x = repmat(1:N,N,1); % generate x-coordinates
y = x'; % generate y-coordinates
% Generate Labels
t = num2cell(M); % extact values into cells
t = cellfun(@(x) num2str(x,3), t, 'UniformOutput', false); % convert to string
% Draw Image and Label Pixels
diagIdx = logical(eye(size(ciHo,1)));
ciHo(diagIdx) = 0; % set diagonal to zero
[row, col] = find(sq(ciHo));
hold on
imagesc(M,[0,1])
colormap(flipud(bone));
plot(row, col, 'r.','MarkerSize',30); % col-0.2 with text
set(gca, 'YDir','reverse');
%text(x(:), y(:), t, 'HorizontalAlignment', 'Center');
xticks([1:nfreq]); yticks([1:nfreq]);
xticklabels(C); yticklabels(C);
xtickangle(90); % rotate x axis labels
xlim([0.5,5.5]);
ylim([0.5,5.5]);
set(gca,'box','off')
set(gca,'XAxisLocation','Top');
set(get(gca,'XAxis'), 'FontSize',24, 'FontWeight', 'bold');
set(get(gca,'YAxis'), 'FontSize',24, 'FontWeight', 'bold');
c = colorbar('Ticks',[0,0.5,1],'TickLabels',[0,0.5,1], 'FontSize',24, 'FontWeight', 'bold');
c.Label.String = 'Correlation';
axis square
header = sprintf('topo correlation between freq bands - avg env/corr');
%title(header);

drawnow;

% save figure
sname = sprintf('%s_Fig3B',savedir);
print('-dpng',sname);


% next two topos -  only compute numbers

% compute correlation again but WITHOUT 12-16 Hz!!
% envelope tracking between envs/cars -------------------------------------
CorrS = zeros(nsub,nenv,nenv);
VARtmp = sq(mean(VAR,3)); % average freq because we arent interested
VARtmp = permute(VARtmp,[2,1,3]);
for s = 1:nsub
    topos = sq(VARtmp(s,:,:))';
    CorrS(s,:,:) = corr(topos);
end
CorrSZ = corr2z(CorrS); % fisher transform
meanCorrSZ = sq(mean(CorrSZ,1));

% randomize creates distribution of means 
% check confidence intervals if they exclude zero
hoCorrSZ = zeros(draws,nenv,nenv);
tic;
for draw = 1:draws
    dcorr = zeros(nsub,nenv,nenv);
    ridx = randi(nsub,1,nsub); % get random index with repetition
    for n = 1:nsub
        rtopos = sq(mean(VAR(:,ridx(n),:,:),3))';
        dcorr(n,:,:) = corr(rtopos);
    end
    dcorr = corr2z(dcorr);
    hoCorrSZ(draw,:,:) = sq(mean(dcorr,1));
end
prcHoS = sq(prctile(hoCorrSZ,1,1));
toc;

ciHoS = prcHoS > 0; % find all cells with CI that exclude 0


% print results
fprintf('------------------------------------- \n \n');
fprintf('Stimulus topography correlations: \n');
sq(mean(CorrS,1))
fprintf('\n');
fprintf('significance label: \n');
ciHoS
fprintf('\n\n');

% p value for envelope correlation
meanC = sq(mean(CorrS,1));
cvalC = hoCorrSZ(:,1,2);
p = sum(cvalC < 0)/draws;
d = mean(cvalC)/std(cvalC);
fprintf('\n');
fprintf('topo car low vs car high\n');
fprintf('corr: %f  pval: %f  cd: %f\n',meanC(1,2),p,d);

% p value for carrier correlation
cvalE = hoCorrSZ(:,3,4);
p = sum(cvalE < 0)/draws;
d = mean(cvalE)/std(cvalE);
fprintf('topo env low vs env high\n');
fprintf('corr: %f    pval: %f cd: %f\n',meanC(3,4),p,d);
fprintf('\n');


%% check bootstrap tracking

figure
iter = 1;
for c = 1:size(VAR,1)
    for b = 1:nfreq
        handle = subplot(4,nfreq,iter);
        pos = get(handle,'position');
        
        % plot MI true and boot
        truemi = sq(avgVARtrue(c,b,:));
        plot(truemi,'b-');
        hold on
        plot(sq(mean(avgVARboot(c,b,:,:),4)),'-','Color',[1,0.5,0.1]);
        
        % plot CI area
        x = [1:128];
        X = [x, fliplr(x)];
        CI = [sq(prctile(avgVARboot(c,b,:,:),5,4))', fliplr(sq(prctile(avgVARboot(c,b,:,:),95,4)))'];
        area = fill(X,CI,[1,0.5,0.1]);
        area.FaceAlpha = 0.4;
        area.LineStyle = 'none';
        
        % put on labels
        if c == 1
            header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(b,1), argout.freq_range(b,2));
            title(header, 'FontSize', 14);
        end
        if b == 1 
            header = sprintf(str{c});
            ylabel(header, 'FontWeight', 'bold', 'FontSize', 14);
        end
        if c == size(VAR,1) && b == 1
            xlabel('Channel', 'FontWeight', 'bold');
        end
        
        drawnow;
        iter = iter + 1;
    end
end

header = sprintf('true MI vs 99 prcnt CI bootstrap MI');
ckfiguretitle(header);





