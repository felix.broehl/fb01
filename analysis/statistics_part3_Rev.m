%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- statistics ---
% part 3 of FB01
% analyzes of behavioral response, i.e. % correct trials
% then compares the conditions with a cluster based permutation test
%
% in the end creates the final plots for submission
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;
clearvars;
% addpath('Y:\Matlab\ckmatlab\eegck');
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results_part3';
finalfigdir = 'C:\Users\fbroehl\Documents\FB01\final_figures';

% parameter ---------------------------------------------------------------
removeSub = 'true';
SingleEnvelope = 'true';    % whether to use data with single env MI or mult env MI
                            % if 'false': compare cons in part 3, all envs
                            % if 'true': compare part 1 data with part 3
                            % data - conditions 1-5 and 8-11
randomization = 'all'; % 'condition', slope', 'all'

draws = 2000;
CHANS = 128;
nfreq = 5;
include = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(include);
tmap = zeros(nfreq,CHANS);
ho = zeros(nfreq,CHANS,draws);


% loading -----------------------------------------------------------------
% load part 1,2,3 data
% CAVE:
% part 1 has 5 conditions
% part 2 has 2 condition
% part 3 has 2 conditions, but each one comes with MI from low and
% high envelope!!!
% see argout.condition
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part1');
Data{1} = MIdata.alldata;
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part2');
Data{2} = MIdata.alldata;
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part3');
Data{3} = MIdata.alldata;

% concatenate all conditions
MI = cat(1, Data{:});
argout = MIdata.argout;
ncomps = 4; % number of comparisons


% prepare layout ----------------------------------------------------------
LAYOUT = 'biosemi128.lay';
cfg = [];
cfg.layout = LAYOUT;
layout = ft_prepare_layout(cfg);

% load head cap structure
load FtDummy128
load neighbourStructure_cap128

% parameters for cluster statistics ---------------------------------------
cfg = [];
cfg.critvaltype = 'par'; %'prctile' % type of threshold to apply. Usual 'par'
cfg.critval = abs(tinv(0.01,nsub-1)); %critical cutoff value for cluster members if parametric
cfg.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfg.clusterstatistic = 'maxsum';
cfg.minsize = 3; % minimal cluster size
cfg.pval = 0.01; % threshold to select signifciant clusters
cfg.df = nsub-1;
cfg.neighbours  = neighbours;




%% final figure part 3 fig 3
% comp topos for env, car and natural/rotated excluding 12-16 Hz, all in
% one figure
% new StatStruc for this figure -------------------------------------------
FigStruc{1}.group1 = 1;
FigStruc{1}.group2 = 8;
FigStruc{1}.label = 'con8-con1';
FigStruc{1}.sname = 'con8-con1';

FigStruc{2}.group1 = 5;
FigStruc{2}.group2 = 9;
FigStruc{2}.label = 'con9-con5';
FigStruc{2}.sname = 'con9-con5';


% plotting ----------------------------------------------------------------
cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlightsize = {10,10};
cfg_topo.highlightsymbol = {'.','.'};
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';
cfg_topo.zlim = [-2.5, 2.5];

% compute stats, save stat values and plot
rowlabel = {'Env_{low}','Env_{high}',['Env_{high}' newline '- Env_{low}']};
figure('Position',[500,100,1250,550]) % set window size
iter = 1;
Cluster = [];
for d = 1:3 %length(FigStruc)
    tmap = zeros(nfreq,CHANS);
    ho = zeros(nfreq,CHANS,draws);
    if d <= 2
        group1con = FigStruc{d}.group1;
        group2con = FigStruc{d}.group2;
        group1 = sq(mean(MI(group1con,:,:,:),1));
        group2 = sq(mean(MI(group2con,:,:,:),1));
        diffAll = group2 - group1;
        diffdiff{d} = diffAll;
    elseif d > 2 % also test diff of diff (is attention effect same for both bands?)
        group1 = diffdiff{1};
        group2 = diffdiff{2};
        diffAll = group2 - group1;
    end
    
    % testing natural against rotated conditions
    tic;
    for freq = 1:nfreq
        diff = sq(diffAll(:,freq,:));
        tmap(freq,:) = mean(diff,1)./(std(diff,[],1)./sqrt(nsub));

        % random permuation ---------------------------------------------------
        switch randomization
            case {'condition', 'all'}
                N = draws;
                for draw = 1:N
                    % invert sign randomly
                    hodiff = diff .* (2*randi([0,1],size(diff))-1);
                    ho(freq,:,draw) = mean(hodiff,1)./(std(hodiff,[],1)/sqrt(nsub));
                end
        end
    end
    toc;
    
    % ck clusterstats -------------------------------------------------
    tic;
    [PosClus,NegClus] = eegck_clusterstats_eeg(cfg,tmap',permute(ho,[2,1,3]));
    toc;
    Cluster{d} = {PosClus,NegClus};
    
    if ~isempty(PosClus)
        nclus = find(PosClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(PosClus.maskSig==c);
            dpr = PosClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(PosClus.p(c)),max(PosClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end

    if ~isempty(NegClus)
        nclus = find(NegClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(NegClus.maskSig==c);
            dpr = NegClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(NegClus.p(c)),max(NegClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end
    
    for freq = 1:nfreq
        cksubplot(4,6,6*(d-1)+freq,1.05);
        EvpDummy.avg =  tmap(freq,:)';
        EvpDummy.time = 0;
        % fetch cluster idx
        posChan = []; negChan = [];
        if ~isempty(PosClus)
            posChan = find(PosClus.maskSig(:,freq));
        end
        if ~isempty(NegClus)
            negChan = find(NegClus.maskSig(:,freq));
        end
        cfg_topo.highlightchannel = {posChan; negChan};
        cfg_topo.highlightcolor = {'r', 'w'};
        ft_topoplotER(cfg_topo,EvpDummy);
        if d == 1
            header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
            title(header, 'FontSize',14);
        end
        if freq == 1
            rowtitle = sprintf(rowlabel{d});
            text(-1.4, 0, rowtitle, 'FontWeight', 'bold', 'FontSize',14);
        end
        drawnow;
        iter = iter + 1;
    end
    
    % colorbar below last topoplot
    if d == 3
        pos = get(gca,'position');
        cbar = colorbar('SouthOutside','Limits',[-2.5,2.5],'Ticks',[-2.5,2.5],'TickLabels',[-2.5,2.5], 'FontSize', 12,'FontWeight', 'bold');
        cbar.Label.String = 't-value';
        set(gca,'position',pos);
        width = cbar.Position(3)/8;
        cbar.Position(1) = cbar.Position(1) + width;
        cbar.Position(3) = cbar.Position(3) - 2*width;
    end
    
    % plot individual diff values averaged over cluster electrodes
    PosData = []; NegData = [];  nclusP = 0; nclusN = 0;
    if ~isempty(PosClus)
        nclusP = length(find(PosClus.p));
        PosData = zeros(nsub,nclusP);
        for c = 1:nclusP
            cluschan = PosClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            cluschan = permute(cluschan, [3,1,2]);
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            PosData(:,c) = mean(B);
        end
    end
    if ~isempty(NegClus)
        nclusN = length(find(NegClus.p));
        NegData = zeros(nsub,nclusN);
        for c = 1:nclusN
            cluschan = NegClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            cluschan = permute(cluschan, [3,1,2]);
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            NegData(:,c) = mean(B);
        end
    end
    % subplot to far right
    if ~isempty(PosClus) || ~isempty(NegClus)
        cksubplot(4,6,6 + 6*(d-1),1.0);
        hold on
        nclus = nclusP + nclusN;
        line([-0.5,nclus+0.5],[0,0],'Color','k', 'LineWidth', 0.8);
        if nclusP > 0
            jitter = 0.2 .* (rand(size(PosData)) - 0.5);
            x = repmat([1:nclusP],size(PosData,1),1);
            x = x + jitter;
            plot(x,PosData,'r.','MarkerSize',7);
        end
        if nclusN > 0
            jitter = 0.2 .* (rand(size(NegData)) - 0.5);
            x = repmat([1:nclusN],size(NegData,1),1);
            x = x + jitter;
            plot(x,NegData,'b.','MarkerSize',7);
        end
        xlim([0.5,8.5]);
        set(gca,'box','off');
        set(get(gca,'XAxis'),'visible','off');
        ticks = get(gca,'YTick');
        ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
        yticks(ticks);
    end
    
end


header = sprintf('tmaps env, part 3, n = 24');
%ckfiguretitle(header);
sname = sprintf('%s/_Fig3',finalfigdir);
% savefig(sname);
% jpgname = sprintf('%s.jpg',sname);
% saveas(gcf,jpgname);
% pngname = sprintf('%s.png',sname);
% saveas(gcf,pngname);
% 
% snameCluster = sprintf('%s/Clusterstats_Fig3',finalfigdir);
% save(snameCluster,'Cluster');


