%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% script to analyze part 1 of the experiment
%
%   ----- script version for PAPER REVISION -----
%
% for each of several freq bins seperately
% 0.2-2, 1-4, 2-6, 4-8, 8-12, 12-16, 16-20 Hz
%
% do frequency analysis and plotting
% loaded EEG data is sampled at 150 Hz
%
% calculate MI between each electrode and the envelope in each
% condition
% conditions (env x carrier) are 
% 1: 1,1  
% 2: 1,3 
% 3: 2,2 
% 4: 3,1 
% 5: 3,3
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


close all;
clearvars;

% which system: LINUX==1; WINDOWS==2
COMP = 2;


% set directories 
root = getRoot;

if COMP==1
    savedir = [root 'analysis/results_boot/'];
    
    % add ck toolbox
    addpath(genpath('/home/fbroehl/FB02/toolboxes/ckmatlab/'));
    % add fieldtrip
    addpath('/home/fbroehl/FB02/toolboxes/fieldtrip-20190905/');
    ft_defaults; % -> really important!
    % add chimera
    addpath('/home/fbroehl/FB02/toolboxes/chimera')
    % add utilities
    addpath('/home/fbroehl/FB02/toolboxes/fb_utils');
    
elseif COMP==2
    savedir = [root 'analysis\results_boot\'];
    
end

manageParpool(-2);


% parameters
goodsubs = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(goodsubs);
random = 'true'; % 'true' to compute Ho randomization
mode = 'MI';
DETAIL = 'mean';    % consider spectral detail of envelope
                    % 'mean', 'single', 'coarse'
                    % use 'coarse' to compute entrianment comparable
                    % between conditions
nfreq = 5;
ncons = 5;
condition = 1:ncons;
CHANS = 128;
draws = 1000;


%% compute entrainment data
for isub = 1:1 %1:nsub 
    allmi = zeros(ncons,nfreq,CHANS);
    allho = zeros(ncons,nfreq,CHANS,draws);
    argout = {}; 
    fprintf('compute %s for avg envelope\n', mode');
    tic;
    for c = 1:1 %1:ncons
        file = goodsubs(isub);
        pause(rand*3); % random wait to facilitate drive access
        
        % do randomization if demanded
        [data,ARG,ho] = computeMI(file,c,draws,random);
        allmi(c,:,:) = data;
        allho(c,:,:,:) = ho;
        argout{c} = ARG;
        ntrl(c) = ARG.numtrl;
    end
    % get ARG.numtrl and assert the trials are complete for each subject
    argout = argout{1};
    toc;
    
    % save data
    sname = sprintf('%s%s_part1_sub%02d.mat',savedir, mode, isub);
    save(sname, 'allmi','allho','argout','ntrl');
end
        


return



%% coherence function
function [out,ARG,ho] = computeMI(file,condition,draws,varargin)
% function to compute PCV/MI for part 1
% not yet implemented

% this function computes the phase coherence value between the trials of
% the chosen condition and the envelope data presented in those trials
%
% by default uses data from EEGPrepro_v3.m aka one ICA over all parts
%
% Parameters:
% file:     
%   files from preprocessed list, integer
% reref:    
%   can be 1 or 2
%   rereferencing eeg data with avereage over all electrodes (1) or
%   mastoid channels (2), integer
% condition:
%   can be 1,2 or 3
%   colculating for either of the three conditions, integer
% USEENVBAND:
%   list indexing which envelopes to be used
%   must fit to the condition - array
% varargin:
%   mode: 'PCV' for phase coherence or 'MI' (default) for mutual information - str
%   randomization: 'false' (default) or 'true' to create a randomized H0 distribution to test
%   for significance - str
%
% Returns:
% out:
%   either PCV or MI computed between eeg and envelopes
% ARG:
%   specifications in a structure
% ho:
%   if randomization was set to 'true', returns a h0 distribution
%

% directories and paths ---------------------------------------------------
root = getRoot;


D{1} = [root 'analysis\clean_data\']; 

envdir{1} = [root 'StimMaterial\Stimuli_3_bands\Block_1'];
envdir{2} = [root 'StimMaterial\Stimuli_3_bands\Block_2'];

% create envelope lists
env_list = [dir(strcat(envdir{1}, '\*.mat')); dir(strcat(envdir{2}, '\*.mat'))];

zcopnorm = @(x) copnorm([real(x) imag(x)]);

% get inputs --------------------------------------------------------------
ARG.part = 1;
ARG.select = condition;                 % which condition
% get variable inputs; mode either PCV (default) or MI
if ~isempty(varargin)
    try
        ARG.randomization = char(varargin(1));
    catch
        ARG.randomization = 'false';
    end
end

% set fixed parameters ----------------------------------------------------
ARG.srate = 150;
ARG.prestim = round(0.8 * ARG.srate); % prestimulus time
ARG.soundlag = round(0.03 * ARG.srate); % soundcard lag
ARG.draws = draws; % n Shifts for bootstrapping
ARG.OFFSET = round(0.5 * ARG.srate); % cut out 500 ms from start because of ERPs 
ARG.lags = round([0.06:0.02:0.14] * ARG.srate); % lags in ms (careful that this works with the sampling rate)
nlags = length(ARG.lags);
CHANS = 128;
ARG.cons = [1,1,2,3,3; 1,3,2,1,3]; % top envelope, bottom carrier
ARG.refchannel = [46, 57, 58, 59, 104, 118, 119, 120]; % mastoid channels

ARG.freq_range = [0.5, 2; 1,4; 2,6; 4,8; 8,12]; % delta, theta, alpha, beta, gamma
nfreq = length(ARG.freq_range);
ARG.nfreq = nfreq;

% create empty variables
MI_true = zeros(nlags,nfreq,CHANS);
MI_shuf = zeros(nfreq,CHANS,ARG.draws);

% filters -----------------------------------------------------------------
ARGF.rate = 150;
ARGF.mode = 'butter';
ARGF.order = 4; % filter order
ARGF.window = 1; % symmetric padding

Filt = {};
for b = 1:nfreq
    ARGF.Freqs = ARG.freq_range(b,:);
    tmp = ck_filt_makefilters(ARGF);
    Filt{b} = tmp{1};
end

% get files ---------------------------------------------------------------
clean_list = dir(strcat(D{1}, '*part_1_CLEAN.mat'));

% load preprocessed data --------------------------------------------------
fprintf('loading file: %s \n', clean_list(file).name);
filename = sprintf('%s/%s', D{1}, clean_list(file).name);
clean_file = load(filename);

% is a cell array because of strings in data structure
numtrl = size(clean_file.TrialInfo,1); % number of present trials

% load trials and concatenate ---------------------------------------------
iter = 1;
env = {};
eeg = {};
for n = 1:numtrl % loop over trials
    trl = clean_file.TrialInfo(n,1); % because some trials might have been rejected!
    idx = clean_file.index_to_envelope(trl,1);
    
    % find condition of the trial (trlcon == 1 to 5)
    trlcon = find((ARG.cons(1,:) == clean_file.index_to_envelope(trl,3)) .* (ARG.cons(2,:) == clean_file.index_to_envelope(trl,2)));
    if trlcon ~= ARG.select
        continue;
    end
    
    % fetch envelope from correct condition list
    envelope_file = sprintf('%s/%s', env_list(idx).folder, env_list(idx).name);
    stimulus_all = load(envelope_file);
    
    % fetch eeg and correct envelope
    env{iter} = stimulus_all.env(clean_file.index_to_envelope(trl,3),:); % find envelope
    eeg{iter} = clean_file.data.trial{trl}(1:CHANS,:);
    N_points_env(iter) = length(env{iter});
    N_points_eeg(iter) = length(eeg{iter});
    
    % reref ----------------------------------------
    eeg{iter} = eeg{iter}-repmat(mean(eeg{iter}(ARG.refchannel,:),1),[128,1]);
    iter = iter + 1;
end

% save number of trials that will be concatenated
ARG.numtrl = length(eeg);

% for efficient processing, we first append, then filter then cut 
env = [env{:}];
eeg = [eeg{:}];

[nenv,~] = size(env);

% start indices for each envelope
env_segment = cumsum(N_points_env)+1;
env_segment = [1, env_segment(1:end-1)];
% start indices for each eeg trial
onset_segment = cumsum(N_points_eeg)+1;
onset_segment = [1 onset_segment(1:end-1)];


for freq = 1:nfreq % loop over frequency ranges
    % filter and hilbert, phase angle later
    brain = ck_filt_applyfilter(eeg,Filt{freq});
    stim = ck_filt_applyfilter(env,Filt{freq});
    stim = ck_filt_hilbert(stim); % complex signal
    brain = ck_filt_hilbert(brain);
    
    % slice with correct offset over all lags
    for lag = 1:nlags
        LAG = ARG.lags(lag);
        
        % first cut env (remove first 500ms for ERPs) ------
        seglen = sum(N_points_env - ARG.OFFSET);
        seg_env = zeros(nenv,seglen);
        s = 0;
        for n = 1:length(N_points_env)
            I_segment = [1:N_points_env(n)] + env_segment(n)-1;
            I_segment = I_segment(ARG.OFFSET+1:end);
            seg_env(:,s+[1:N_points_env(n)-ARG.OFFSET]) = stim(:,I_segment);
            s = s+N_points_env(n)-ARG.OFFSET;
        end
        
        % then cut eeg accordingly -------------------------
        offset = ARG.prestim + ARG.soundlag + ARG.OFFSET + LAG;
        seg_brain = zeros(CHANS,seglen);
        s = 0;
        
        for n = 1:length(N_points_eeg)
            % for each sentence, we select this setence from the long set
            I_segment = [1:N_points_eeg(n)] + onset_segment(n)-1;
            % -> this segment brain(:,I_segment);
            % now add the lag and take as many elements as we have in the envelope
            I_segment = I_segment(offset:offset+N_points_env(n)-1-ARG.OFFSET);
            seg_brain(:,s+[1:N_points_env(n)-ARG.OFFSET]) = brain(:,I_segment);
            s = s+N_points_env(n)-ARG.OFFSET;
        end
        
        
        % compute MI ---------------------------------------------------------
        S_hilc = zcopnorm(seg_env'); % z-transform
        E_hilc = zcopnorm(seg_brain');
        parfor e = 1:CHANS
            MI_true(lag,freq,e) = mi_gg(S_hilc,E_hilc(:,e+[0,128]),'true','true');
        end
    end
    
    
    % h0 distribution -----------------------------------------------------
    % not lag because we assume its causally separated anyways
    N = ARG.draws;
    switch ARG.randomization
        case {'true'}
            nt = length(seg_env);
            parfor draw = 1:N % parfor loop here!
                shift = randi([round(nt/4),round(nt - nt/4)],N,1);
                tmpMI = zeros(1,CHANS);
                for e = 1:CHANS
                    ivec = [1:nt];
                    ivec = circshift(ivec,shift(draw));
                    tmpMI(e) = mi_gg(S_hilc(ivec,:),E_hilc(:,e+[0,CHANS]),'true','true');
                end
                MI_shuf(freq,:,draw) = tmpMI;
            end
    end
    
end

% average/sum over time lags
MI_true = sq(mean(MI_true,1));

% output w/ or w/o randomization 
if nargout < 3
    out = MI_true;
elseif nargout > 2
    out = MI_true;
    ho = MI_shuf;
end


end


%% get root directory function
function root = getRoot

mfile = mfilename('fullpath');
[root,~,~] = fileparts(mfile);

root = root(1:end-8);

end
