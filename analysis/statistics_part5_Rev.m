%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- statistics ---
% first do a t test between all conditions
% load data from part 1 and 2 to complement linear data
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;
clearvars;
addpath('Y:\Matlab\ckmatlab\eegck');
addpath('C:\Users\fbroehl\Documents\FB01\analysis');
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results_part5';
finalfigdir = 'C:\Users\fbroehl\Documents\FB01\analysis\final_figures';

% loading -----------------------------------------------------------------
removeSub = 'true';
randomization = 'all'; % 'condition', slope', 'all'
include = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(include);
CHANS = 128;

MIdataCoarse = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part5_coarse');
MI_coarse = MIdataCoarse.alldata;

% MIdataSubband = load('C:\Users\fbroehl\Documents\MATLAB\FB01\analysis\results_part5\MI_part5_subband');
% MI_subband = MIdataSubband.alldata;


% parameter ---------------------------------------------------------------
comp = [1,2]; % select conditions to compare (1,2 or 3)
argout = MIdataCoarse.argout;
nfreq = size(argout.freq_range,1);
tscore_thrs = abs(tinv(0.01,nsub-1)); % inverse of t cumulative distribution 
draws = 2000;

tmap = zeros(nfreq,CHANS);
ho = zeros(nfreq,CHANS,draws);
hoBetaBands = zeros(nfreq,CHANS,draws);
hoBetaMI = zeros(nfreq,CHANS,draws);

% prepare layout ----------------------------------------------------------
LAYOUT = 'biosemi128.lay';
cfg = [];
cfg.layout = LAYOUT;
layout = ft_prepare_layout(cfg);

% load head cap structure
load FtDummy128
load neighbourStructure_cap128

% custom channel map
load ChanMap
frontcent = eegck_returnEIndex(ChanMap.frontcent,layout.label);
occipital = eegck_returnEIndex(ChanMap.occipital,layout.label);

% ck clusterstats -----------------------------------------------------
cfg = [];
cfg.critvaltype = 'par'; %'prctile' % type of threshold to apply. Usual 'par'
cfg.critval = abs(tinv(0.01,nsub-1)); %critical cutoff value for cluster members if parametric
cfg.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfg.clusterstatistic = 'maxsum';
cfg.minsize = 3; % minimal cluster size
cfg.pval = 0.01; % threshold to select signifciant clusters
cfg.df = nsub-1;
cfg.neighbours  = neighbours;

% plot
groupview = 0;
tmapraw = 'false';


%% compute regression
% behaviour
xsc = ones(size(meanBHVR)) .* [3,6,12];
LinReg = @(w1,w2, t) w1 * t + w2;
x = [3,6,12; 1,1,1]';
% betas for nBands/Behaviour
w = x\meanBHVR';
wMean = mean(w,2);

% EEG data 
% all channels
eegAll = MI;
meanEEGall = sq(mean(eegAll,4));

% betas for nBands/MI
for freq = 1:nfreq
    wEEGnBand(:,freq,:) = x\sq(meanEEGall(:,freq,:));
end
% wEEGnBandavg = sq(mean(wEEGnBand,2));


% betas for behaviour/MI
for freq = 1:nfreq
    for s = 1:nsub
        for c = 1:CHANS
            indVar = [meanBHVR(s,:); 1,1,1]';
            depVar = eegAll(:,freq,c,s);
            wEEGBhvr(:,freq,c,s) = indVar \ depVar;
        end
    end
end
% take mean over subjects
wEEGavg = sq(mean(wEEGBhvr,4));
wEEGavg = sq(wEEGavg(1,:,:));
% compute t score against zero mean
betaTscore = sq(mean(wEEGBhvr(1,:,:,:),4))./sq(std(wEEGBhvr(1,:,:,:),[],4)./sqrt(nsub));
betaSig = betaTscore;
betaSig(find(betaTscore<=tscore_thrs)) = 0;
% take mean over channels and subjects
wSub = sq(mean(wEEGBhvr,4));
wSubMean = sq(mean(wSub,3));

% betas for nBands/MI
for freq = 1:nfreq
    for s = 1:nsub
        for c = 1:CHANS
            indVar = [3,6,12; 1,1,1]';
            depVar = eegAll(:,freq,c,s);
            wMiBands(:,freq,c,s) = indVar \ depVar;
        end
    end
end

% wMiBandsAvg = sq(mean(wMiBands,2));
% wMiBandsAvg = sq(wMiBandsAvg(1,:,:));

% compute t score against zero mean
betaTscoreMI = sq(mean(wMiBands(1,:,:,:),4))./sq(std(wMiBands(1,:,:,:),[],4)./sqrt(nsub));

switch randomization
        case {'slope', 'all'}
            for freq = 1:nfreq
                N = draws;
                for draw = 1:N
                    % invert sign randomly
                    hodiff = betaTscore(freq,:) .* (2*randi([0,1],size(betaTscore(freq,:)))-1);
                    hoBetaBands(freq,:,draw) = mean(hodiff)./(std(hodiff)/sqrt(nsub));
                    hodiff = betaTscoreMI(freq,:) .* (2*randi([0,1],size(betaTscoreMI(freq,:)))-1);
                    hoBetaMI(freq,:,draw) = mean(hodiff)./(std(hodiff)/sqrt(nsub));
                end
            end
end



%% cluster test slopes 
% ck clusterstats -----------------------------------------------------
fprintf('compute cluster stats');
tic;
[PosClusBands,NegClusBands] = eegck_clusterstats_eeg(cfg,betaTscore',permute(hoBetaBands,[2,1,3]));
[PosClusMI,NegClusMI] = eegck_clusterstats_eeg(cfg,betaTscoreMI',permute(hoBetaMI,[2,1,3]));
ClusterBands = {PosClusBands,NegClusBands};
ClusterMI = {PosClusMI,NegClusMI};
toc;



%% plot regression

% plot behavioural regression ---------------------------------------------
ts = 3:1:12;
figure
hold on
% sublot(1,2,1);
plot(xsc,meanBHVR, 'ro');
plot(xsc, mean(meanBHVR), 'bx', 'MarkerSize', 16);
plot(ts, LinReg(w(1,:)',w(2,:)', ts)', 'color', [0,0,0,0.2], 'LineWidth', 1);
plot(ts, LinReg(wMean(1),wMean(2), ts), 'b', 'LineWidth', 2);
xlim([2, 13]);
ylim([0, 1.2]);
xlabel('number of envelopes');
ylabel('% correct answers');
grid on
header = sprintf('nBands/BHVR linear regression - n = %d', nsub);
ckfiguretitle(header);
sname = sprintf('%s/linreg_bandsBHVR', savedir);
savefig(sname);



%% topo plots -------------------------------------------------------------

cfg_topo = [];
cfg_topo.layout = 'biosemi128.lay';
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'nearest'; 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.highlightsymbol    = 'o';
cfg_topo.highlightcolor     = [1,1,1];
cfg_topo.highlight          = 'off';
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.markersymbol = '.';
cfg_topo.highlightsize = {3,3};
cfg_topo.highlightsymbol = 'o';
cfg_topo.highlightcolor = [1,0,0];
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';


% tmaps and significance map for slopes in all channel --------------------
cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlightsize = {9,9};
cfg_topo.highlightsymbol = {'.','.'};
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';
cfg_topo.zlim = [-2.5, 2.5];

nfreq = 5; % disregard 12-16 Hz range

% tmaps and significance map for slopes in all channel --------------------
figure('Position',[400,100,1200,900]) % set window size
for freq = 1:nfreq
    cksubplot(2,5,freq,1.2);
    EvpDummy.avg =  betaTscore(freq,:)';
    EvpDummy.time = 0;
    posChan = []; negChan = [];
    if ~isempty(PosClusBands)
        posChan = find(PosClusBands.maskSig(:,freq));
    end
    if ~isempty(NegClusBands)
        negChan = find(NegClusBands.maskSig(:,freq));
    end
    cfg_topo.highlightchannel = {posChan; negChan};
    cfg_topo.highlightcolor = {'r', 'w'};
    ft_topoplotER(cfg_topo,EvpDummy);
    header = sprintf('Bhvr/MI, %1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
    title(header);
    
    cksubplot(2,5,freq+5,1.2);
    EvpDummy.avg =  betaTscoreMI(freq,:)';
    EvpDummy.time = 0;
    posChan = []; negChan = [];
    if ~isempty(PosClusMI)
        posChan = find(PosClusMI.maskSig(:,freq));
    end
    if ~isempty(NegClusMI)
        negChan = find(NegClusMI.maskSig(:,freq));
    end
    cfg_topo.highlightchannel = {posChan; negChan};
    cfg_topo.highlightcolor = {'r', 'w'};
    ft_topoplotER(cfg_topo,EvpDummy);
    header = sprintf('nBands/MI');
    title(header);
    drawnow;
end
header = sprintf('top: Bhvr/MI bottom: nBands/MI - t scores of slope against zero mean, n = %d', nsub);
ckfiguretitle(header);
sname = sprintf('%s/tmap_linreg_MI_part5', savedir);
savefig(sname);

sname = sprintf('%s/_Fig5_part5', finalfigdir);
savefig(sname);

% save cluster stats
snameCluster = sprintf('%s/Clusterstats_Fig5',finalfigdir);
save(snameCluster,'ClusterBands','ClusterMI');


% we take the significant electrodes and average their trajectory later
% probably better to just define a frontal and an occipital are by hand
% frChan = find(NegClusMI{3}.maskSig);
% ocChan = find(PosClusMI{1}.maskSig);
% 
% frMI = MI(:,:,frChan,:);
% ocMI = MI(:,:,ocChan,:);

% plot MI trajectory in clusters with significant slope -------------------
frMI = MI(:,:,frontcent,:);
ocMI = MI(:,:,occipital,:);
frDATA = sq(mean(frMI,3));
ocDATA = sq(mean(ocMI,3));
frDATAavg = sq(mean(mean(frMI,3),4));
ocDATAavg = sq(mean(mean(ocMI,3),4));
SEMfr = sq(std(frDATA,[],3)/sqrt(nsub));
SEMoc = sq(std(ocDATA,[],3)/sqrt(nsub));
    
figure
for freq = 1:nfreq
    subplot(2,3,freq);
    hold on
    errorbar(frDATAavg(:,freq),SEMfr(:,freq), 'LineWidth',2);
    errorbar(ocDATAavg(:,freq),SEMoc(:,freq), 'LineWidth',2);
    xlabel('condition');
    xticks([1,2,3]);
    xlim([0.5,3.5]);
    ylabel('MI');
    header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
    title(header);
    legend({'frontal', 'occipital'});
    grid on
    drawnow;
end
header = sprintf('MI trajectory in significance clusters');
ckfiguretitle(header);
sname = sprintf('%s/MI_trajectory', savedir);
savefig(sname);



%% compare averaged envelope MI to averaged MI (sanity check)

% figure
% for freq = 1:nfreq
%     data = [];
%     subplot(3,2,freq);
%     data{1} = sq(mean(MI(1,freq,frontcent,:),3));
%     data{2} = sq(mean(MI_subband{1}(1,freq,frontcent,:),3));
%     data{3} = sq(mean(mean(MI_subband{2}([1:2],freq,frontcent,:),1),3));
%     data{4} = sq(mean(mean(MI_subband{3}([1:4],freq,frontcent,:),1),3));
%     boxplot([data{:}]);
% end
% header = sprintf('MI of averaged envelope vs averaged MI - variance over subj');
% ckfiguretitle(header);
% sname = sprintf('%s/MI_averaged_comparison', savedir);
% savefig(sname);



%% statistical difference between high and low env tracking in all conditions
% ENVhigh - ENVlow for all conditions
% analogous to part 1
nfreq = 5;

cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlightsize = {9,9};
cfg_topo.highlightsymbol = {'.','.'};
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';
cfg_topo.zlim = [-2.5, 2.5];


rowlabel = {'3Bands','6Bands','12Bands'}; 

figure('Position',[200,200,1200,800])
for k = 1:3 % condition loop
    tmap = zeros(nfreq,CHANS);
    ho = zeros(nfreq,CHANS,draws);
    group1con = 1 + (k-1)*3;
    group2con = 3 + (k-1)*3;
    group1 = sq(mean(MI_coarse(group1con,:,:,:),1));
    group2 = sq(mean(MI_coarse(group2con,:,:,:),1));
    diffAll = group2 - group1;
    % testing natural against rotated conditions
    tic;
    for freq = 1:nfreq
        diff = sq(diffAll(:,freq,:));
        tmap(freq,:) = mean(diff,1)./(std(diff,[],1)./sqrt(nsub));

        % random permuation ---------------------------------------------------
        N = draws;
        for draw = 1:N
            % invert sign randomly
            hodiff = diff .* (2*randi([0,1],size(diff))-1);
            ho(freq,:,draw) = mean(hodiff,1)./(std(hodiff,[],1)/sqrt(nsub));
        end
    end
    toc;
    
    tic;
    [PosClus,NegClus] = eegck_clusterstats_eeg(cfg,tmap',permute(ho,[2,1,3]));
    toc;
    
    % print cluster stats and p value
    if ~isempty(PosClus)
        nclus = find(PosClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(PosClus.maskSig==c);
            dpr = PosClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.3f  tsum=%2.2f  \n',c,min(PosClus.p(c)),max(PosClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end

    if ~isempty(NegClus)
        nclus = find(NegClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(NegClus.maskSig==c);
            dpr = NegClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.3f  tsum=%2.2f  \n',c,min(NegClus.p(c)),max(NegClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end
    
    % plotting
    for freq = 1:nfreq
        cksubplot(4,6,6*(k-1) + freq,1.1);
        EvpDummy.avg =  tmap(freq,:)';
        EvpDummy.time = 0;
        cfg_topo.highlightchannel = [];
        % fetch cluster idx
        posChan = []; negChan = [];
        if ~isempty(PosClus)
            posChan = find(PosClus.maskSig(:,freq));
        end
        if ~isempty(NegClus)
            negChan = find(NegClus.maskSig(:,freq));
        end
        cfg_topo.highlightchannel = {posChan; negChan};
        cfg_topo.highlightcolor = {'r', 'w'};
        ft_topoplotER(cfg_topo,EvpDummy);
        if k == 1
            header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
            title(header, 'FontSize',14);
        end
        if freq == 1
            %header = sprintf(rowlabel{k});
            text(-1.2, 0, rowlabel{k}, 'FontWeight', 'bold', 'FontSize',15);
        end
        drawnow;
    end
    
    % plot individual diff values averaged over cluster electrodes
    PosData = []; NegData = [];  nclusP = 0; nclusN = 0;
    if ~isempty(PosClus)
        nclusP = length(find(PosClus.p));
        PosData = zeros(nsub,nclusP);
        for c = 1:nclusP
            cluschan = PosClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            PosData(:,c) = mean(B);
        end
    end
    if ~isempty(NegClus)
        nclusN = length(find(NegClus.p));
        NegData = zeros(nsub,nclusN);
        for c = 1:nclusN
            cluschan = NegClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            NegData(:,c) = mean(B);
        end
    end
    % subplot to far right
    if ~isempty(PosClus) || ~isempty(NegClus)
        cksubplot(4,6,6 + 6*(k-1),1.0);
        hold on
        nclus = nclusP + nclusN;
        line([-0.5,nclus+0.5],[0,0],'Color','k', 'LineWidth', 0.8);
        if nclusP > 0
            jitter = 0.2 .* (rand(size(PosData)) - 0.5);
            x = repmat([1:nclusP],size(PosData,1),1);
            x = x + jitter;
            plot(x,PosData,'r.','MarkerSize',7);
        end
        if nclusN > 0
            jitter = 0.2 .* (rand(size(NegData)) - 0.5);
            x = repmat([1:nclusN],size(NegData,1),1);
            x = x + nclusP + jitter;
            plot(x,NegData,'b.','MarkerSize',7);
        end
        xlim([0.5,8.5]);
        set(gca,'box','off');
        set(get(gca,'XAxis'),'visible','off');
        ticks = get(gca,'YTick');
        ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
        yticks(ticks);
    end
    
end
pos = get(gca,'position');
cbar = colorbar('SouthOutside','Limits',[-2.5,2.5],'Ticks',[-2.5,2.5],'TickLabels',[-2.5,2.5], 'FontSize', 12,'FontWeight', 'bold');
cbar.Label.String = 't-value';
caxis([-2.5,2.5]);
set(gca,'position',pos);
width = cbar.Position(3);
width = width*2/3;
cbar.Position(3) = width;



%% next rows errorbars -----------------------------------------------------

% MI trajectory for all three envelopes in one figure
% create two separate figures for clarity
label = {'fr','oc'};
% from violinplot in freq_analysis_envelopes.m
barcolor = [0,0.5,0.75; 0.2,0.5,0.2; 0.55,0.2,0.42];

compList = [1,4,7];
RegX = [1,2,3; 1,1,1]';

figure
hold on
for d = 1:3 % plot only full errorbars
    % loop for low envelope and high envelope slope stats
    xAxis = RegX(:,1)';
    getCond = compList + (d-1);
    compMat = MI_coarse(getCond,:,:,frontcent);
    frdataAvg = sq(mean(mean(compMat,4),2));
    % SEM across subjects
    data = sq(mean(compMat,4));
    frdataSEM = sq(std(data,[],2)/sqrt(nsub));
    
    iter = 1;
    for freq = 1:nfreq
        % actual plotting -------------------------------------------------
        subplot(1,6,freq); % third row)
        hold on
        if d == 2 % mid env
            errorbar(xAxis,frdataAvg(:,freq),frdataSEM(:,freq),'--','Color',barcolor(d,:), 'LineWidth',1.5);
        else
            errorbar(xAxis,frdataAvg(:,freq),frdataSEM(:,freq),'Color',barcolor(d,:), 'LineWidth',2.5);
        end
        %xlabel('condition');
        xticks([1:3]);
        xticklabels([3,6,12]);
        xlim([0.5,3 + 0.5]);
        if freq == 1
            xlabel('number of bands', 'FontWeight', 'bold');
            ylabel('MI', 'FontWeight', 'bold');
            header = sprintf(label{1});
            %text(-0.9, 2.0, header, 'FontWeight', 'bold', 'FontSize',15);
        end
        %header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
        %title(header);
        %legend(label);
        grid off
        if d == 3
            % only min max yticks
            ticks = get(gca,'YTick');
            ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
            yticks(ticks);
        end
        drawnow;
        iter = iter + 1;
    end
end

% set one legend outside
pos = get(gca,'position');
legendlabel = {'low','mid','high'};
legend(legendlabel, 'Location', 'southeastoutside');
set(gca,'position',pos);


