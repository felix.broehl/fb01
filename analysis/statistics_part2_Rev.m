%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% --- statistics ---
% part 2 of FB01
% analyzes of behavioral response, i.e. % correct trials
% then compares the conditions with a cluster based permutation test
%
% in the end creates the final plots for submission
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%close all;
clearvars;
addpath('Y:\Matlab\ckmatlab\eegck');
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis\results';
finalfigdir = 'C:\Users\fbroehl\Documents\FB01\analysis\final_figures';

% loading -----------------------------------------------------------------
randomization = 'all'; % 'condition', slope', 'all'
include = [3,5:6,8:16,18:27,30:31]; % index subjects to include
nsub = length(include);
nfreq = 5;

MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part1');
data1 = MIdata.alldata;
MIdata = load('C:\Users\fbroehl\Documents\FB01\analysis\results\MI_part2');
data2 = MIdata.alldata;
MI = cat(1,data1,data2);


% parameter ---------------------------------------------------------------
argout = MIdata.argout;
tscore_thrs = tinv(0.99,nsub-1); % inverse of t cumulative distribution with p of 0.95 and 20 degrees of freedom (nsub-1)
draws = 2000;
CHANS = 128;
tmap = zeros(nfreq,CHANS);
ho = zeros(nfreq,CHANS,draws);

% prepare layout ----------------------------------------------------------
LAYOUT = 'biosemi128.lay';
cfg = [];
cfg.layout = LAYOUT;
layout = ft_prepare_layout(cfg);

% load head cap structure
load FtDummy128
load neighbourStructure_cap128


% parameters for cluster statistics ---------------------------------------
cfg = [];
cfg.critvaltype = 'par'; %'prctile' % type of threshold to apply. Usual 'par'
cfg.critval = abs(tinv(0.01,nsub-1)); %critical cutoff value for cluster members if parametric
cfg.conn = 8; % connectivity criterion (for 2D 4 or 8  
cfg.clusterstatistic = 'maxsum';
cfg.minsize = 3; % minimal cluster size
cfg.pval = 0.01; % threshold to select signifciant clusters
cfg.df = nsub-1;
cfg.neighbours  = neighbours;



%% final figure part 1,2 fig 2
% comp topos for env, car and natural/rotated excluding 12-16 Hz, all in
% one figure

% new StatStruc for this figure -------------------------------------------
FigStruc{1}.group1 = [1,4];
FigStruc{1}.group2 = [2,5];
FigStruc{1}.label = 'Comp Car mean Env';
FigStruc{1}.sname = 'Comp_Car_mean_Env';

FigStruc{2}.group1 = [1,2];
FigStruc{2}.group2 = [4,5];
FigStruc{2}.label = 'Comp Env mean Car';
FigStruc{2}.sname = 'Comp_Env_mean_Car';

FigStruc{3}.group1 = [1,5];
FigStruc{3}.group2 = [2,4];
FigStruc{3}.label = 'rotated minus natural';
FigStruc{3}.sname = 'Comp_Rot_Nat';

FigStruc{4}.group1 = 1;
FigStruc{4}.group2 = 6;
FigStruc{4}.label = 'LEnv mult - LEnv single';
FigStruc{4}.sname = 'LEnvMult-LEnvSingle';

FigStruc{5}.group1 = 4;
FigStruc{5}.group2 = 7;
FigStruc{5}.label = 'HEnv mult - HEnv single';
FigStruc{5}.sname = 'HEnvMult-HEnvSingle';

% plotting ----------------------------------------------------------------
cfg_topo = [];
cfg_topo.layout = LAYOUT;
cfg_topo.fontsize = 6;
cfg_topo.colorbar = 'no';
cfg_topo.interpolation = 'v4'; % nearest for realer plots, v4 for nicer plots
cfg_topo.style   = 'both';
cfg_topo.comment  = 'no';
cfg_topo.marker = 'off';
cfg_topo.highlightsize = {9,9};
cfg_topo.highlightsymbol = {'.','.'};
cfg_topo.highlight = 'on';
cfg_topo.shading = 'flat';
cfg_topo.zlim = [-2.5, 2.5];

% compute stats, save stat values and plot
rowlabel = {'?Car','?Env','Inter','Env_{low}^{2Car}','Env_{high}^{2Car}'};
figure('Position',[400,100,1250,900]) % set window size
iter = 1;
Cluster = [];
for d = 1:5
    tmap = zeros(nfreq,CHANS);
    ho = zeros(nfreq,CHANS,draws);
    group1con = FigStruc{d}.group1;
    group2con = FigStruc{d}.group2;
    group1 = sq(mean(MI(group1con,:,:,:),1));
    group2 = sq(mean(MI(group2con,:,:,:),1));
    diffAll = group2 - group1;
    % testing natural against rotated conditions
    tic;
    for freq = 1:nfreq
        diff = sq(diffAll(:,freq,:));
        tmap(freq,:) = mean(diff,1)./(std(diff,[],1)./sqrt(nsub));

        % random permuation ---------------------------------------------------
        N = draws;
        for draw = 1:N
            % invert sign randomly
            hodiff = diff .* (2*randi([0,1],size(diff))-1);
            ho(freq,:,draw) = mean(hodiff,1)./(std(hodiff,[],1)/sqrt(nsub));
        end
    end
    toc;
    
    % ck clusterstats -------------------------------------------------
    tic;
    [PosClus,NegClus] = eegck_clusterstats_eeg(cfg,tmap',permute(ho,[2,1,3]));
    toc;
    Cluster{d} = {PosClus,NegClus};
    
    if ~isempty(PosClus)
        nclus = find(PosClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(PosClus.maskSig==c);
            dpr = PosClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(PosClus.p(c)),max(PosClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end

    if ~isempty(NegClus)
        nclus = find(NegClus.p);
        fprintf('------------------------------------------------------------------\n');
        for c = nclus
            range = find(NegClus.maskSig==c);
            dpr = NegClus.Effect(c);
            dpr = dpr/sqrt(nsub);
            fprintf('Clus %d   p=%1.4f  tsum=%2.2f  \n',c,min(NegClus.p(c)),max(NegClus.stat(c)));
            fprintf('Cohen d %1.2f \n',dpr);
        end
        fprintf('------------------------------------------------------------------\n\n');
    end
    
    for freq = 1:nfreq
        cksubplot(6,6,6*(d-1)+freq,1.2);
        EvpDummy.avg =  tmap(freq,:)';
        EvpDummy.time = 0;
        % fetch cluster idx
        posChan = []; negChan = [];
        if ~isempty(PosClus)
            posChan = find(PosClus.maskSig(:,freq));
        end
        if ~isempty(NegClus)
            negChan = find(NegClus.maskSig(:,freq));
        end
        cfg_topo.highlightchannel = {posChan; negChan};
        cfg_topo.highlightcolor = {'r', 'w'};
        ft_topoplotER(cfg_topo,EvpDummy);
        if d == 1
            header = sprintf('%1.1f - %1.1f Hz', argout.freq_range(freq,1), argout.freq_range(freq,2));
            title(header, 'FontSize',14);
        end
        if freq == 1
            header = sprintf(rowlabel{d});
            text(-1.4, 0, header, 'FontWeight', 'bold', 'FontSize',15);
        end
        drawnow;
        iter = iter + 1;
    end
    
    % colorbar below last topoplot
    if d == 5
        pos = get(gca,'position');
        cbar = colorbar('SouthOutside','Limits',[-2.5,2.5],'Ticks',[-2.5,2.5],'TickLabels',[-2.5,2.5], 'FontSize', 12,'FontWeight', 'bold');
        cbar.Label.String = 't-value';
        set(gca,'position',pos);
        width = cbar.Position(3)/8;
        cbar.Position(1) = cbar.Position(1) + width;
        cbar.Position(3) = cbar.Position(3) - 2*width;
    end
    
    % sum up three clusters (with same effect) in first row
    if d == 2
        PosClus.p = PosClus.p([1,2,3]);
        PosClus.stat = PosClus.stat([1,2,3]);
        PosClus.mask(PosClus.mask==4) = 1;
        PosClus.Effect = PosClus.Effect([1,2,3]);
        PosClus.maskSig(PosClus.maskSig==4) = 1;
    end
        
    
    % plot individual diff values averaged over cluster electrodes
    PosData = []; NegData = [];  nclusP = 0; nclusN = 0;
    if ~isempty(PosClus)
        nclusP = length(find(PosClus.p));
        PosData = zeros(nsub,nclusP);
        for c = 1:nclusP
            cluschan = PosClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            cluschan = permute(cluschan, [3,1,2]);
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            PosData(:,c) = mean(B);
        end
    end
    if ~isempty(NegClus)
        nclusN = length(find(NegClus.p));
        NegData = zeros(nsub,nclusN);
        for c = 1:nclusN
            cluschan = NegClus.maskSig==c;
            clussize = length(find(cluschan));
            cluschan = repmat(cluschan',[1,1,nsub]); % corret dim ord
            cluschan = permute(cluschan, [3,1,2]);
            B = diffAll(cluschan);
            B = reshape(B,[clussize,nsub]);
            NegData(:,c) = mean(B);
        end
    end
    % subplot to far right
    if ~isempty(PosClus) || ~isempty(NegClus)
        cksubplot(6,6,6 + 6*(d-1),1.0);
        hold on
        nclus = nclusP + nclusN;
        line([-0.5,nclus+0.5],[0,0],'Color','k', 'LineWidth', 0.8);
        if nclusP > 0
            jitter = 0.2 .* (rand(size(PosData)) - 0.5);
            x = repmat([1:nclusP],size(PosData,1),1);
            x = x + jitter;
            plot(x,PosData,'r.','MarkerSize',7);
        end
        if nclusN > 0
            jitter = 0.2 .* (rand(size(NegData)) - 0.5);
            x = repmat([1:nclusN],size(NegData,1),1);
            x = x + jitter;
            plot(x,NegData,'b.','MarkerSize',7);
        end
        xlim([0.5,8.5]);
        set(gca,'box','off');
        set(get(gca,'XAxis'),'visible','off');
        ticks = get(gca,'YTick');
        ticks = [ticks(1), ticks(end)/2+ticks(1)/2, ticks(end)];
        yticks(ticks);
    end
end

header = sprintf('tmaps env, carr, inter, enhance low, enhance high');
%ckfiguretitle(header);
sname = sprintf('%s/_Fig2',finalfigdir);
% savefig(sname);
% pngname = sprintf('%s.png',sname);
% saveas(gcf,pngname);
% 
% snameCluster = sprintf('%s/Clusterstats_Fig2',finalfigdir);
% save(snameCluster,'Cluster');



