% load envelopes used in study FB01, thendetect onsets in every envelope
% band
% then for every freq enevelope, for every onset check if there are onsets
% in the other bands in time window (maybe 150 ms before and after) and put
% them into time bins
% show histogram of onsets.
% then we can see if correlation of onsets is independent of corretaion of
% freq envelopes


% define functions and variables

%close all;
clearvars;

% add functions and packages
addpath('Y:\Matlab\chimera');
addpath('Z:\FB00\fb_utils');


% find all sentence files
file_dir = 'Z:\FB01\experiment\Stimuli\all';
target_dir = 'C:\Users\fbroehl\Documents\FB01\StimMaterial\Stimuli_12_bands\all';
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis_stimuli\results_onset_corr';
finalfigdir = 'C:\Users\fbroehl\Documents\FB01\analysis\final_figures';

nbands = 12;
fco = equal_xbm_bands(200,8000,nbands);

% onset corr cfg
cfg.minProm = 0.33; % find peak
cfg.minDist = 19; % find peak
cfg.nbins = 5; % number of bins in the radius (one-sided)
cfg.bin_width = 6; % sample points per bin ~ 40 ms; must be divisible by 2
cfg.fs = 150; % sample rate of envs
cfg.random = 'true'; % create ho
cfg.draws = 2000; % optional, default is 2000

mat_list = dir(sprintf('%s/*.mat',target_dir));


%% compute Onset Corr
% functions below
tic;
[bins,ho] = OnsetCorr(cfg, mat_list);
toc;


%% evaluation
xx = 0.5:11.5;
yy = 0.5:12.5;
[X1,Y1] = meshgrid(xx,yy);
[X2,Y2] = meshgrid(yy,xx);

figure
% x Axis tick labels
bin_length = cfg.bin_width * (1/cfg.fs);
xAxis = ((1:2*cfg.nbins+1) - (cfg.nbins+1)) * bin_length;

for k = 1:nbands
    mat = sq(bins(k,:,:));
    mat(k,:) = 0; % remove onsets in same band
    matHo = sq(ho(:,k,:,:));
    pidx = zeros(size(bins));
    for i = 1:12
        for j = 1:11
            if mat(i,j) > prctile(sq(matHo(:,i,j)),99)
                pidx(k,i,j) = 1;
            end
        end
    end
    
    mat = mat./sum(sum(mat)); % all bars sum up to 1
    subplot(4,4,k);
    hold on
    
    matalt = ones(size(mat));
    matalt(k,:) = 0.6;
    
    colormap bone
    imagesc(matalt, [0,1]);
    
    
    % mark sign. bars
    [col,row] = find(sq(pidx(k,:,:)));
    plot(X1,Y1,'k-');
    plot(Y2,X2,'k-');
    plot(row, col,'LineStyle','none', 'MarkerSize',8, 'Marker', 'square', 'MarkerFaceColor', 'r', 'MarkerEdgeColor', 'none'); % col-0.2 with text
    %set(gca, 'YDir','reverse');
    xlim([0.5,11.5]);
    ylim([0.5,12.5]);
    %xticklabels(xAxis);
    xticklabels([]);
    yticklabels([]);
    
    header = sprintf('env %d',k);
    title(header);
    axis square
end
if k == 9
    xlabel('bucket (s)');
    ylabel('envelope');
end
% pos = get(gca,'position');
% cbar = colorbar('SouthOutside','Limits',[0,1],'Ticks',[0,0.1],'TickLabels',[0,0.1], 'FontSize', 12,'FontWeight', 'bold');
% cbar.Label.String = 'likelihood';
% set(gca,'position',pos);

return
    
    
if 1 == 0
    % x Axis tick labels
    bin_length = cfg.bin_width * (1/cfg.fs);
    xAxis = ((1:2*cfg.nbins+1) - (cfg.nbins+1)) * bin_length;
    % create bar graphs for bins
    for k = 1:nbands
        figure
        mat = sq(bins(k,:,:));
        mat(k,:) = 0; % remove onsets in same band
        mat = mat./sum(sum(mat)); % all bars sum up to 1
        b = bar3(mat);
        for n = 1:length(b) % colorcode bar plot for height
            zdata = b(n).ZData;
            b(n).CData = zdata;
            b(n).FaceColor = 'interp';
        end
        xticklabels(xAxis);
        xlabel('bucket (s)');
        ylabel('envelope');
        zlim([0,0.1])
        header = sprintf('envelope %02d',k);
        ckfiguretitle(header);
        sname = sprintf('%s/Onset_corr_env_%02d',savedir,k);
        %savefig(sname);
        jpgname = sprintf('%s/Onset_corr_env_%02d.jpg',savedir,k);
        %saveas(gcf,jpgname);
    end
end

fprintf('done evaluating \n');

%% final figure
% only show three onset hist plots

fromenv = [2,6,10];
bin_length = cfg.bin_width * (1/cfg.fs);
xAxis = ((1:2*cfg.nbins+1) - (cfg.nbins+1)) * bin_length;
figure('Position',[100,100,1600,600]);
for k = 1:3
    cksubplot(1,3,k,1.05);
    nenv = fromenv(k);
    mat = sq(bins(nenv,:,:));
    mat(nenv,:) = 0; % remove onsets in same band
    mat = mat./sum(sum(mat)); % all bars sum up to 1
    b = bar3(mat);
    for n = 1:length(b) % colorcode bar plot for height
        zdata = b(n).ZData;
        b(n).CData = zdata;
        b(n).FaceColor = 'interp';
    end
    xlim([0,12]);
    xticklabels(xAxis);
    xtickangle(-35);
    ylim([0,13]);
    yticklabels(round(fco(1:end-1) + diff(fco)/2));
    ytickangle(20);
    zticks([0,0.05,0.1]);
    zlim([0,0.1])
    
    a = get(gca,'YTickLabels');
    set(gca,'YTickLabel',a,'FontSize',13,'FontWeight','bold');
    
    xlabel('time bin (s)', 'FontSize',14, 'FontWeight','bold', 'Rotation', 20);
    ylabel(['center' newline 'frequency (Hz)'], 'FontSize',14, 'FontWeight','bold', 'Rotation', -35);
    zlabel('probability', 'FontSize',14, 'FontWeight','bold');
    grid off
    header = sprintf('%4.0f - %4.0f Hz',fco(nenv),fco(nenv+1));
    title(header, 'FontSize', 16);
end

% save figure
sname = sprintf('%s/Onset_Hist', finalfigdir);
%savefig(sname);
pngname = sprintf('%s.png',sname);
%saveas(gcf,pngname);




%% load envelope files and create correlation statistics
function [data,ho] = OnsetCorr(cfg,list)
mat_list = list;

if strcmp(cfg.random,'true')
    if ~isfield(cfg,'draws')
    cfg.draws = 2000;
    end
else
    cfg.draws = 1;
end

% go through all files, find peaks in first derivative
OnsetCell = {};
OnsetCellShift = {};
for i = 1:length(mat_list)
    load(sprintf('%s/%s',mat_list(i).folder,mat_list(i).name));
    nbands = size(env,1);
    envlen(i) = size(env,2);
    env = env ./ max(env,[],2);
    envd = diff(env, [], 2);
    envd = envd ./ max(envd,[],2);
    for k = 1:size(envd,1)
        [~, locs] = findpeaks(envd(k,:), 'MinPeakProminence', cfg.minProm, 'MinPeakDistance', cfg.minDist);
        OnsetCell{i}{k} = locs;
    end
    
    % randomize onsets with cirshift
    if strcmp(cfg.random,'true')
        imin = round(envlen(i) * 0.25);
        imax = round(envlen(i) * 0.75);
        shift = randi([imin,imax],nbands,cfg.draws);
        for j = 1:cfg.draws
            % first shift each subenv individually, then find peaks
            for k = 1:nbands
                locs = OnsetCell{i}{k};
                locs = fb_circidx(locs,shift(k,j),envlen(i)); % shifts indices around envlen
                OnsetCellShift{i}{k}(j,:) = locs;
            end
        end
    end
            
end

% compute corr
bins = onsetlikelihood(cfg, OnsetCell, nbands, 1);
bins_shuf = onsetlikelihood(cfg, OnsetCellShift, nbands, cfg.draws);


fprintf('done calculating results \n');

data = bins;

if nargout == 2
    if strcmp(cfg.random,'true')
        ho = bins_shuf;
    end
end

end

%%
function data = onsetlikelihood(cfg, Input, nbands, Ndraws)
% go through all onsets and find onsets in other bands
bins = zeros(Ndraws, nbands, nbands, 2*cfg.nbins+1);
maxwin = cfg.nbins * cfg.bin_width + cfg.bin_width * 0.5;
floorfix = @(x)floor(abs(x)).*sign(x);


for i = 1:length(Input)
    % go through all freq bands
    for k = 1:nbands
        onsets = Input{i}{k};
        %tmpbin = zeros(nbands, cfg.nbins);
        % go through all onsets
        if ~isempty(onsets)
            for j = 1:Ndraws
                for n = 1:size(onsets,2)
                    % check in all other freq envelopes
                    for m = 1:nbands
                        addbins = zeros(1,1,1,2*cfg.nbins+1); % fit ndims to bins
                        nextonsets = Input{i}{m}(j,:);
                        dif = nextonsets - onsets(j,n);
                        difCut = dif(dif < maxwin & dif > - maxwin);
                        difN = floorfix(difCut/cfg.bin_width);
                        difN = difN + cfg.nbins + 1;
                        addbins(difN) = 1;
                        bins(j,k,m,:) = bins(j,k,m,:) + addbins;
                    end
                end
            end
        end
    end
end

data = sq(bins); % remove singleton dimension

end
