%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% script to analyze frequency spectra of the envelopes used in FB01
% superplot envelopes with different spectral detail
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all;
clearvars;

% directories and paths ---------------------------------------------------
addpath('Y:\Matlab\ckmatlab\ckstatistics');
addpath('Y:\Matlab\ckmatlab\ckbox');
savedir = 'C:\Users\fbroehl\Documents\FB01\analysis_stimuli\results';
finalfigdir = 'C:\Users\fbroehl\Documents\FB01\final_figures';

envdir{1} = 'Z:\FB01\experiment\Stimuli_3_bands\all';
envdir{2} = 'Z:\FB01\experiment\Stimuli_6_bands\all';
envdir{3} = 'Z:\FB01\experiment\Stimuli_12_bands\all';

% paramters ---------------------------------------------------------------
ARG.env_fs = 150;   % envelope sample rate
ARG.window = 150;
ARG.noverlap = round(ARG.window/2);
ARG.minProm = 0.33;
ARG.minDist = 19;

% create envelope list
env_list{1} = dir(strcat(envdir{1}, '\*.mat')); 
env_list{2} = dir(strcat(envdir{2}, '\*.mat')); 
env_list{3} = dir(strcat(envdir{3}, '\*.mat')); 

nEnv = length(env_list{1});
onsets.Diff = {};
for k = 1:3
    onsets.Diff{k} = [];
end

%% create PSD estimate with welch's method
CorrStructure = [];
Stimuli = {};
for k = 1:length(env_list{1})
    filename = sprintf('%s/%s', env_list{1}(k).folder, env_list{1}(k).name); 
    load(filename);
    
    
    % correlation of subbands with total envelope
    ENV = mean(env);
    nband = size(env,1);
    for i = 1:nband
        envn = env(i,:);
        coef = corrcoef(ENV,envn);
        CorrStructure(k,i) = coef(1,2);
    end

    % correcting for 1/f power law
    %Z = zscore(env');
    E{k} = env;
    Z = env';
    %Z = resample(Z,44100,150);
    [Pxx, F] = pwelch(Z,ARG.window,ARG.noverlap,ARG.env_fs);
    %[Pxx, F] = pwelch(Z,32,16,128);
    spctrm_tmp(k,:,:) = Pxx;
    
    % peak detection
    env_length(k,:) = length(env);
    env = env ./ max(env,[],2);
    envd = diff(env, [], 2);
    envd = envd ./ max(envd,[],2);
    for n = 1:size(envd,1)
        % specifiy min width or min distance!
        [~, locs] = findpeaks(envd(n,:), 'MinPeakProminence', ARG.minProm, 'MinPeakDistance', ARG.minDist);
        loc_idx{k}{n} = locs;
        onsets.num_locs(k,n) = length(locs);
        onsets.window(k,n) = locs(end) - locs(1);
        onsets.Diff{n} = [onsets.Diff{n}, diff(locs)];
    end
end
onsets.locs = loc_idx;
onsets.Hz = (onsets.num_locs-1)./(onsets.window/ARG.env_fs);
onsets.Fs = ARG.env_fs;
spctrm = cat(3,spctrm_tmp, mean(spctrm_tmp,3));
mean_spctrm = squeeze(mean(spctrm,1));

sname = sprintf('%s/OnsetStats',savedir);
%save(sname,'onsets');

duration = env_length ./ ARG.env_fs;
totaldur = sum(duration);

fprintf('total length of stimulus material per condition:\n');
fprintf('%f seconds\n\n',totaldur);



% plot mean PSD -----------------------------------------------------------
Env = [E{:}]';
% normalize each env
for k = 1:3
    Env(:,k) = Env(:,k)/max(Env(:,k));
end

[Pxx, F] = pwelch(Env,256,128,512,40);
figure
hold on
for k = 1:3
    plot(F,log(Pxx(:,k)));
end


%idx = randi(nEnv);
idx = 13;
for b = 1:length(env_list)
    filename = sprintf('%s/%s', env_list{b}(idx).folder, env_list{b}(idx).name); 
    load(filename);
    env = mean(env)/max(mean(env));
    envMat(:,b) = env;
end

%% average correlation coefficients 12 envelopes
CF = []; 
for k = 1:length(env_list{3})
    filename = sprintf('%s/%s', env_list{3}(k).folder, env_list{3}(k).name); 
    load(filename);
    CF(k,:,:) = corrcoef(env');
end
cf = sq(mean(CF,1));
imagesc(cf);
axis square
colorbar
header = sprintf('corr coef between 12 envelopes, averaged over sentences');
ckfiguretitle(header);


%% correlation of each subband with the grand envelope
CorrStruc = {};
for n = 1:length(env_list)
    for k = 1:length(env_list{n})
        filename = sprintf('%s/%s', env_list{n}(k).folder, env_list{n}(k).name); 
        load(filename);
        ENV = mean(env);
        nband = size(env,1);
        sbandCorr = [];
        for i = 1:nband
            coef = corrcoef(ENV,env(i,:));
            sbandCorr(:,i) = coef(1,2);
        end
        CorrStruc{n}(k,:) = sbandCorr;
    end
end

figure
pos = [1,2,4];
for n = 1:length(env_list)
    subplot(1,6,[pos(n):pos(n)+n-1]);
    violinplot(CorrStruc{n});
    nbands = size(CorrStruc{n},2);
    fco = equal_xbm_bands(200,8000,nbands) / 1000;
    fco = fco(1:end-1) + diff(fco)/2; % get center frequency
    ticks = arrayfun(@(x) sprintf('%.2g',x),fco,'UniformOutput',false);
    xticklabels(ticks);
    xlabel('Envelope center freq (kHz)');
    ylabel('corrcoef');
end
header = sprintf('correlation of each subenvelope with the grand average envelope');
ckfiguretitle(header);



%% plot power spectral density

% plot one examplary PSD --------------------------------------------------
figure 
hold on;
for k = 1:size(mean_spctrm,2)
    subplot(2,2,k);
    plot(squeeze(mean_spctrm(:,k)));
    grid('on');
    %xlabel('frequency (Hz)');
end
hold off;


% plot onsets histograms --------------------------------------------------
titlelist = ['HML'];
figure;
hold on
plotidx = [1,4,7];
for k = 1:3
    subplot(3,3,[plotidx(k), plotidx(k)+1]);
    upsdown = 4-k;
    histogram(onsets.Diff{upsdown}./150, 'BinWidth', 0.01, 'Normalization', 'probability');
    xlim([0,2]);
    xlabel('time (s)');
    ylim([0,0.1]);
    yticks([0.0, 0.05, 0.1]);
    ylabel('norm. number');
    title(titlelist(k));
end
DiffMean(k) = mean(onsets.Diff{k})/150;
DiffStd(k) = std(onsets.Diff{k})/150;

subplot(3,3,[3,6,9]);
% as boxplot
x1 = onsets.Diff{1}/150;    % convert to second
x2 = onsets.Diff{2}/150;
x3 = onsets.Diff{3}/150;
x = ([x1, x2, x3]);
g1 = repmat({'Low'},size(x1'));
g2 = repmat({'Mid'},size(x2'));
g3 = repmat({'High'},size(x3'));
g = [g1; g2; g3];       % grouping variable for multiple boxplot in one
boxplot(x,g);
xlabel('envelope');
ylabel('length (s)');
header = sprintf('onset window distribution for each envelope');
ckfiguretitle(header);
sname = sprintf('%s/%s', savedir, 'onset_window_distribution');
%savefig(sname);



% plot random sentence with onset locations -------------------------------
load(sprintf('%s/%s', env_list{1}(idx).folder, env_list{1}(idx).name));
env = env ./ max(env,[],2);
envd = diff(env, [], 2);
envd = envd ./ max(envd,[],2);
t = 1/ARG.env_fs:1/ARG.env_fs:length(env)/ARG.env_fs;
figure;
title(sprintf('sentence number %d', idx));
hold on

subplot(4,3,[1,2]);
% plot envelopes with different spectral detail overlaid each other
% s = (length(envMat)/ARG.env_fs) - 1/ARG.env_fs;
% t = 0:1/ARG.env_fs:s;
plot(t, envMat);
xlim([0,t(end)]);
xlabel('time (s)');
ylim([-0.1,1.1]);
set(gca,'box','off')
set(get(gca,'YAxis'),'visible','off') % invisible y axis
legend({'3 bands,', '6 bands', '12 bands'});

% plot subband envelope with onsets
for k = 1:3
    subplot(4,3,[3*k+1,3*k+2]);
    hold on
    upsdown = 4-k;
    plot(t,env(upsdown,:));
    plot(t(loc_idx{idx}{upsdown}), env(upsdown,loc_idx{idx}{upsdown}), 'r*');
    hold off;
    xlim([0,t(end)]);
    ylim([-0.1,1.1]);
    set(get(gca,'YAxis'),'visible','off') % invisible y axis
end

subplot(4,3,[3,6,9,12]);
boxplot(onsets.Hz);
xticklabels({'Low', 'Mid', 'High'});
xlabel('envelope');
ylabel('Hz');
ckfiguretitle('onset frequency of three envelopes of all sentences');
sname = sprintf('%s/stimuli', savedir);
%savefig(sname);

MeanHz = mean(onsets.Hz);
StdHz = std(onsets.Hz);
fprintf('------------------------------------------------------------------\n');
fprintf('low    mid     high\n');
fprintf('mean:\n');
fprintf('%1.2f   %1.2f    %1.2f  Hz\n',MeanHz);
fprintf('std:\n');
fprintf('%1.2f   %1.2f    %1.2f  Hz\n',StdHz);
fprintf('------------------------------------------------------------------\n');



% plot envelopes with different spectral detail overlaid each other -------
s = (length(envMat)/ARG.env_fs) - 1/ARG.env_fs;
t = 0:1/ARG.env_fs:s;
figure
plot(t, envMat);
xlim([0,t(end)]);
xlabel('time (s)');
legend({'3 bands,', '6 bands', '12 bands'});
grid on
header = sprintf('envelope overlay');
ckfiguretitle(header);
sname = sprintf('%s/envelope_overlay', savedir);
%savefig(sname);


%% compute subband autocorrelation
win = 2; % 3 seconds
maxlag = win * ARG.env_fs;
winlen = 2*maxlag+1;
ntrl = 39;
nband = 3;
autocorr = zeros(ntrl,winlen,nband);
bandnames = {'Env_{low}','Env_{mid}','Env_{high}'};


for trl = 1:length(E)
    [tmp,lags] = xcorr(E{trl}',maxlag,'coeff');
    tmp = tmp(:,[1,5,9]);
    autocorr(trl,:,:) = tmp;
end
lags = lags ./ ARG.env_fs;

figure
for band = 1:3
    subplot(1,3,band);
    plot(lags,sq(autocorr(:,:,band)), 'b-');
    hold on
    plot(lags, sq(mean(autocorr(:,:,band),1)), 'k-', 'LineWidth', 2);
    axis square
    xlim([-win,win])
    if band == 1
        xlabel('Time (s)','FontSize',12,'FontWeight','bold');
    end
    xticks([-win:0.5:win]);
    yticks([0:0.2:1]);
    header = sprintf(bandnames{band});
    title(header,'FontSize',16);
end
header = sprintf('Normalized subband autocorrelation');
ckfiguretitle(header);




%% statistical testing
[P,Tabs,Stats] = anova1(onsets.Hz);
if P < 0.05
    c = multcompare(Stats)
end

% correlation test
[P,Tabs,Stats] = anova1(CorrStructure);
if P < 0.05
    c = multcompare(Stats)
end


%% plot final figure - Fig. S
color = [0,0.5,0.75; 0.2,0.5,0.2; 0.55,0.2,0.42];
figure('Position',[600,350,1300,550])

% total env
pos = [1,2];
name = {'Env_{Total}'};
subplot(4,4,pos);
stim = sq(mean(env));
stim = stim ./ max(stim);
plot(t,stim,'k-','LineWidth',1.2);
xlim([0,t(end)]);
xticklabels([]);
ylim([-0.1,1.1]);
box off
set(get(gca,'YAxis'),'visible','off') % invisible y axis
header = name;
text(-1.0,0,header,'FontSize',16,'FontWeight','bold','Rotation',90);

% subbands
pos = [5,6;9,10;13,14];
name = {'Env_{High}', 'Env_{Mid}', 'Env_{Low}'};
for k = 1:3
    subplot(4,4,[pos(k,:)]);
    hold on
    upsdown = 4-k;
    plot(t,env(upsdown,:),'Color',color(upsdown,:),'LineWidth',1.2);
    plot(t(loc_idx{idx}{upsdown}), env(upsdown,loc_idx{idx}{upsdown}), 'r*','LineWidth',1.5);
    hold off;
    xlim([0,t(end)]);
    if k < 3
        xticklabels([]);
    end
    ylim([-0.1,1.1]);
    set(get(gca,'YAxis'),'visible','off') % invisible y axis
    header = name{k};
    text(-1.0,0,header,'FontSize',16,'FontWeight','bold','Rotation',90);
end
xlabel('Time (s)', 'FontSize', 16,'FontWeight','bold');
a = get(gca,'XTickLabel');
%set(gca,'XTickLabel',a,'FontSize',12);


x1 = onsets.Diff{1}/150;    % convert to second
x2 = onsets.Diff{2}/150;
x3 = onsets.Diff{3}/150;
x = ([x1, x2, x3]);
g1 = repmat({'Low'},size(x1'));
g2 = repmat({'Mid'},size(x2'));
g3 = repmat({'High'},size(x3'));
g = [g1; g2; g3];       % grouping variable for multiple boxplot in one

% plot boxplots onset freq ------------------------------------------------
subplot(4,4,[3,7,11,15]);
%boxplot(onsets.Hz);
vio = violinplot(onsets.Hz,g, 'ShowData',true, 'ShowNotches',false, 'GroupOrder',{'Low','Mid','High'}, 'ViolinAlpha', 0.5, 'EdgeColor',[0.2,0.2,0.2]);
for k = 1:3
    vio(k).ViolinColor = color(k,:);
end
yticks([1:5]);
ylim([0.5,5.5]);
set(gca,'XTickLabel',{'Low', 'Mid', 'High'},'FontSize',14,'FontWeight','bold');
xlabel('Envelope', 'FontSize', 16,'FontWeight','bold');
ylabel('Frequency (Hz)', 'FontSize', 16,'FontWeight','bold');
set(gca,'box','off')

% plot VIOLIN plots onset window distribution
subplot(4,4,[4,8,12,16]);
vio = violinplot(CorrStructure, g, 'ShowData',true, 'ShowNotches',false, 'GroupOrder',{'Low','Mid','High'}, 'ViolinAlpha', 0.5, 'EdgeColor',[0.2,0.2,0.2]);
for k = 1:3
    vio(k).ViolinColor = color(k,:);
end
yticks([0,0.5,1]);

a = get(gca,'YTickLabel');
set(gca,'YTickLabel',a,'FontSize',20,'FontWeight','bold');
set(gca,'XTickLabel',{'Low', 'Mid', 'High'},'FontSize',14,'FontWeight','bold');
xlabel('Envelope', 'FontSize', 16,'FontWeight','bold');
ylabel('Correlation', 'FontSize', 16,'FontWeight','bold');
set(gca,'box','off')

sname = sprintf('%s/_Fig2AB',savedir);
%savefig(sname);
pngname = sprintf('%s.png',sname);
saveas(gcf,pngname);



