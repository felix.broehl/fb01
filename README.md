# FB01

Speech and speech-like sound tracking in human EEG.

Bröhl, F., Kayser, C., 2021. Delta/theta band EEG differentially tracks low and high frequency speech-derived envelopes. Neuroimage 233, 117958. https://doi.org/10.1016/j.neuroimage.2021.117958
